MkBundle+
=========

Make Bundle Plus (MkBundle+) es una aplicación desarrollada como Proyecto Final de Carrera (PFC) en la  [Universidad de La Laguna][ULL] para el Grupo de Investigación ‘Evolución de galaxias en el Grupo Local’, del [Instituto Astrofísico de Canarias][IAC].

DisPar utiliza la plataforma [Bitbucket Cloud][Bitbucket Cloud] para alojar el repositorio remoto privado, disponible en este [enlace][Remote Repository]. Si se desea obtener privilegios en este repositorio, contactar con los autores. Lo ususarios inexpertos en esta plataforma pueden consultar: [Bitbucket Cloud Documentation][Bitbucket Cloud Documentation].

Detalles
--------

Esta aplicación es una interfaz gráfica (GUI) para crear el fichero de entrada de un código para calcular historias de formación estelar.

El lenguaje base del desarrollo de esta aplicación es [Python][Python]. Se utilizo [PyQt][PyQt] como binding de [Qt][Qt].

Software
--------

Los requisitos básicos para usar y desarrollar esta aplicación son:

1. Tener instalado un interprete Python en su versión _2.7.11_ para nuestro sistema operativo.
1. Disponer de los siguientes módulos Python:
    * **SIP**. La versión usada es la _4.16.9_.
    * **PyQt4**. La versión usada es la _4.10.4_.
    * **ipython**. La versión usada es la _3.2.0_.
    * **matplotlib**. La versión usada es la _1.4.3_.
    * **scipy**. La versión usada es la _0.17.1_.
    * **pyfits**. La versión usada es la _3.3_.
    * **numpy**. La versión usada es la _1.9.2_.
    * **numexpr**. La versión usada es la _2.4.3_.
    * **packaging**. La versión usada es la _15.3_.
    * **sympy**. La versión usada es la _0.7.6.1.3_.
1. Tener accesible el comando `pyrcc4` en la variable `PATH` del sistema.

##### Sugerencia #####

Se suguiere la instalación de [Anaconda][Anaconda] en el _home_ del usuario. [Anaconda][Anaconda] es una distribución [Python][Python] que proporciona un instalador gratuito fácil de usar, quecontiene una colección de más de 720 módulos de código abierto. El uso de [Anaconda][Anaconda] no es obligatorio, sin embargo, es muy recomendable puesto que es muy fácilde usar y evitará realizar otras configuraciones de forma manual. La mayoría de los módulos requeridos son instalados por defecto. Sin embargo, los módulos **pyfits**, **pyqt** (alias del módulo **PyQt4**) y **sip** (alias del módulo **SIP**) no están incluídos por defecto. La versión del instalador [Anaconda][Anaconda] que se ha usado es la _2.3.0_.

##### Instalación #####

La instalación básica, sin usar el software sugerido, comprende los siguientes puntos:

1. **[Python][Python]**: se puede obtener en su [página oficial](https://www.python.org/downloads/), eligiendo el sistema operativo y versión _2.7.11_. El proceso de instalación es el habitual para el sistema operativo elegido.

1. **Módulos**: para instalar los módulos especificados el usuario o desarrollador pueden consultar la documentación oficial de [Python][Python], disponiblen en la siguiente [dirección web](https://docs.python.org/2.7/installing/). Para facilitar esta labor se ha ofrecido el fichero `requirements.txt` localizado en el directorio `/gui/configuration/`.


La instalación del entorno usando [Anaconda][Anaconda], comprende los siguientes pasos:

1. **[Anaconda][Anaconda]**: la información de cómo descargar e instalar [Anaconda][Anaconda] se puede obtener, consultando su documentación oficial, disponible en la siguiente [dirección web](https://docs.continuum.io/anaconda/install). En el proceso de instalación es importante elegir la modificación o personalización de la variable `PATH` de nuestro sistema operativo.

1. El resto de pasos se abordarán en mayor detalle en este documento en la **configuración**.

##### Configuración #####

Si se ha elegido instalar [Anaconda][Anaconda] es necesario seguir los pasos indicados en la documentación oficial, disponible en la [dirección web](http://conda.pydata.org/docs/using/envs.html#use-environment-from-file), junto el fichero `conda-environment.yml` localizado en el directorio `/gui/configuration/` para replicar el entorno. Además, se ha facilitado el script `conda-create-environment.sh` localizado en el mismo directorio, que muestra los pasos indicados en la documentación oficial. El script creará un entorno con los mismos paquetes y versiones al ser ejecutado.

Para una instalación manual de los módulos python en el anaconda se puede hacer lo siguiente:

* Para instalar el módulo **pyfits**:

    1. Abrir una interfaz de línea de comandos (CLI).

    1. Ejecutar el comando:
    ```sh
    pip install pyfits
    ```

* Para instalar el módulo **PyQt4**:

    1. Abrir una interfaz de línea de comandos (CLI).

    1. Ejecutar el comando:
    ```sh
    conda install pyqt
    ```

Después, es necesario asegurarse que la ruta del programa `pyrcc4` esté en la variable `PATH` de nuestro sistema. Se ha facilitado un script, llamado `pyrcc4-check.py` localizado en `/gui/configuration/` para sistemas GNU\Linux, que comprueba la accesibilidad de este programa.

Uso
---

Para ejecutar la aplicación sólo es necesario ejecutar el comando:
```sh
python run.py
```

Autores
-------

* [Anthony Vittorio Russo Cabrera](https://www.linkedin.com/in/anthonyrussocabrera)
* [Manuel Luis Aznar](https://es.linkedin.com/in/manuel-luis-aznar-6723758a)

[ULL]: <http://www.ull.es/>
[IAC]: <http://iac.es/>
[Bitbucket Cloud]: <https://bitbucket.org/>
[Bitbucket Cloud Documentation]: <https://confluence.atlassian.com/bitbucket>
[Remote Repository]: <https://bitbucket.org/ULLfypIAC/crowding-effects-gui>

[Python]: <https://www.python.org/>
[PyQt]: <https://riverbankcomputing.com/software/pyqt/intro>
[Qt]: <https://www.qt.io/>
[Anaconda]: <https://www.continuum.io/downloads>
#!/usr/bin/env python
#
# Script python para Linux y OS X.
#
# Comprueba la accesibilidad del
# comando 'pyrcc4'

import subprocess

process = subprocess.Popen(["which", "pyrcc4"], stdout=subprocess.PIPE)
process.wait()
if process.returncode != 0:
    print("Debes incluir 'pyrcc4' en la variable PATH del sistema")

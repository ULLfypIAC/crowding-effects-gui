#!/usr/bin/env bash
#
# Script para Linux, OS X.
#
# Crea un entorno conda idéntico al
# entorno con el que se desarrolló.

# Obtener el directorio donde se encuentra
# el fichero con las especificaciones del entorno
_directory=$(realpath $(dirname $0))

# Crear entorno
conda env create                                 \
    --file "${_directory}/conda-environment.yml" \
    --name dev_env                               \
    --force

# Activar entorno
source activate dev_env

# Desactivar entorno
#source deactivate
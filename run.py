#!/usr/bin/python
# -*- coding: utf-8 -*-

def main():
    # Obtener la ruta absoluta
    import os
    path = os.getcwd()
    # Borrar ficheros de recursos
    from sources.resources.make import remove_all_resources
    remove_all_resources(path=path)
    # Generar ficheros fuentes de los recursos
    from sources.resources.make import compile_all_resources
    compile_all_resources(path=path)
    # Volver al directorio de la aplicación
    os.chdir(path)
    # Lanzar aplicación
    from sources import start_app as app
    app.main()


if __name__ == '__main__':
    main()

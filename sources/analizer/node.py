#!/usr/bin/python
# -*- coding: utf-8 -*-

""" Modulo que implementa a la Clase Nodo"""


class Nodo():
    """Metodos de la Clase Nodo"""

    def __init__(self, valor, izquierdo=None, derecho=None):
        """ Constructor de la Clase Nodo que asigna el valor del nodo
            asigna su hijo izquierdo y su hijo derecho"""
        self.valor = valor
        self.izquierdo = izquierdo
        self.derecho = derecho

    def es_hoja(self):
        """ Metodo que comprueba si un nodo es hoja"""
        return ((self.izquierdo == None) and (self.derecho == None))

    def es_unario(self):
        """ Metodo que comprueba si un nodo es unario (solo tiene hijo
            derecho"""
        return ((self.izquierdo == None) and (self.derecho != None))

#!/usr/bin/python
# -*- coding: utf-8 -*-

"""UNIVERSIDAD DE LA LAGUNA
 ETSII 
 3 ING TEC INFORMATICA
 COMPILADORES. PRACTICA 8. Traduccion dirigida por sintaxis

 Fecha:    Noviembre de 2011
 Archivo:  parser.py
 Autores:  Ricardo Urdin  Mata 4173
           Manuel Luis Aznar
           Ruben Cabrera

 Descripcion:  Implementa un analizador sintactico descendente recursivo
   con recuperacion de errores que al evaluar las acciones semanticas
   asociadas con las producciones de la gramatica especificada, (ver ETDS),
   produce como resultado un arbol sintactico abstracto (AST) de la expresion
   analizada. 

 ETDS:
       1. Expr ::= Term
                    {Expr_aux.h := Term.ptr} Expr_aux {Expr.ptr := Expr_aux.s}
       2. Expr_aux ::= PLUS Term
                        {Expr_aux1.h := mknode(PLUS, Expr_aux.h, Term.ptr)}
                        Expr_aux1 {Expr_aux.s := Expr_aux1.s}
       3. Expr_aux ::= MINUS Term
                        {Expr_aux1.h := mknode(MINUS, Expr_aux.h, Term.ptr)}
                        Expr_aux1 {Expr_aux.s := Expr_aux1.s}
       4. Expr_aux ::= EPSILON {Expr_aux.s := Expr_aux.h}
       5. Term ::= Factor {Term_aux.h := Factor.ptr} Term_aux
                            {Term.ptr := Term_aux.s}
       6. Term_aux ::= ASTERISK Factor 
                      {Term_aux1.h := mknode(ASTERISK, Term_aux.h, Factor.ptr)}
                      Term_aux1 {Term_aux.s := Term_aux1.s}
       7. Term_aux ::= DIVIDE Factor 
                        {Term_aux1.h := mknode(DIVIDE, Term_aux.h, Factor.ptr)}
                        Term_aux1 {Term_aux.s := Term_aux1.s}
       8. Term_aux ::= EPSILON {Term_aux.s := Term_aux.h}
       9. Factor ::= LEFTPARENTHESIS Expr RIGHTPARENTHESIS
                        {Factor.ptr := Expr.ptr}
       10. Factor ::= MINUS Factor1 {Factor.ptr := mkunode(MINUS, Factor1.ptr)}
       11. Factor ::= ID {Factor.ptr := mkleaf(ID, ID.ptr)}
       12. Factor ::= NUM {Factor.ptr := mkleaf(NUM, NUM.ptr}
       13. Factor ::= FLOAT {Factor.ptr := mkleaf(FLOAT, FLOAT.ptr)}
"""

from sources.analizer import node, tree, lexer
from sources.constant.Constants import Const as C


class Syntax:
    """Clase que implementa el analizador sintactico"""

    def __init__(self, exp):
        """ Constructor: recibe como parametro un string con la
            expresion a analizar"""
        self.lexer = lexer.Lex(exp)
        self.lookahead = self.lexer.yylex()
        self.error = 0
        self.arbol = tree.Arbol()

    def columna_error(self):
        """ metodo que devuelve la longuitud de un "lexema" analizado como error
            Su funcion es situar el error encontrado al principio del error"""
        if (self.lookahead[0] == C.t_ENDTEXT):
            return 0
        else:
            return len(str(self.lookahead[1]))

    def syntax_error(self, stop):
        """ metodo que imprime por pantalla un mensaje indicando
            el error encontrado asi como la posicion del error en
            el codigo fuente y un descripcion del error"""
        self.error = 1
        # if (self.lookahead[0] == C.t_TOKEN_ERROR):
        #    print "Lexical Error:"
        #    print "Linea: ", self.lookahead[2], "\tColumna: ", self.lookahead[3], "\tDescripcion: ", self.lookahead[1]
        # else:
        #    columna = self.lexer.num_columna - self.columna_error()
        #    print "Syntax Error:"
        #    print "Linea: ", self.lexer.num_linea, "\tColumna: ", columna, "\tNo se esperaba: ", self.lookahead[0]

        while (not (self.lookahead[0] in stop)):
            self.lookahead = self.lexer.yylex()

    def match(self, token_esperado, stop):
        """ Consume un Token si es el esparado, y actualiza el lookahead.
            Si no es el esperado manda error"""
        if (self.lookahead[0] == token_esperado):
            self.lookahead = self.lexer.yylex()
            self.syntax_check(stop)
        else:
            self.syntax_error(stop)

    def syntax_check(self, stop):
        """ Comprueba que el token a analizar esta en el conjunto de parada"""
        if (not (self.lookahead[0] in stop)):
            self.syntax_error(stop)

    def inicio(self):
        """ metodo que arranca el analisis de una expresion.
            Comenzamos llamando al metodo correspondiente al
            simbolo de arranque
            Este llamara de forma recursiva al resto de metodos necesarios y
            construira el AST correspondiente a la expresion."""
        self.arbol.raiz = self.expr(set([C.t_ENDTEXT]))
        self.match(C.t_ENDTEXT, set([C.t_ENDTEXT]))
        # if not self.error:
        #    print "Analisis sintactico finalizado correctamente"

    def expr(self, stop):
        """1. Expr ::= Term {Expr_aux.h := Term.ptr} 
                       Expr_aux {Expr.ptr := Expr_aux.s}"""
        expr_ptr = None
        term_ptr = self.term(stop | C.first_EXPR_AUX)
        expr_aux_h = term_ptr
        expr_aux_s = self.expr_aux(stop, expr_aux_h)
        expr_ptr = expr_aux_s
        return expr_ptr

    def expr_aux(self, stop, expr_aux_h):
        """2. Expr_aux ::= PLUS
                    Term {Expr_aux1.h := mknode(PLUS, Expr_aux.h, Term.ptr)}
                    Expr_aux1 {Expr_aux.s := Expr_aux1.s} |
                           MINUS
                    Term {Expr_aux1.h := mknode(MINUS, Expr_aux.h, Term.ptr)}
                    Expr_aux1 {Expr_aux.s := Expr_aux1.s} |
                           EPSILON {Expr_aux.s := Expr_aux.h}"""
        expr_aux_s = None
        if (self.lookahead[0] == C.t_PLUS):
            self.match(C.t_PLUS, stop | C.first_TERM | C.first_EXPR_AUX)
            term_ptr = self.term(stop | C.first_EXPR_AUX)
            expr_aux1_h = node.Nodo(C.c_PLUS, expr_aux_h, term_ptr)
            expr_aux1_s = self.expr_aux(stop, expr_aux1_h)
            expr_aux_s = expr_aux1_s
        elif (self.lookahead[0] == C.t_MINUS):
            self.match(C.t_MINUS, stop | C.first_TERM | C.first_EXPR_AUX)
            term_ptr = self.term(stop | C.first_EXPR_AUX)
            expr_aux1_h = node.Nodo(C.c_MINUS, expr_aux_h, term_ptr)
            expr_aux1_s = self.expr_aux(stop, expr_aux1_h)
            expr_aux_s = expr_aux1_s
        else:
            expr_aux_s = expr_aux_h
            self.syntax_check(stop)
        return expr_aux_s

    def term(self, stop):
        """3. Term ::= Factor {Term_aux.h := Factor.ptr}
                    Term_aux {Term.ptr := Term_aux.s}"""
        term_ptr = None
        factor_ptr = self.factor(stop | C.first_TERM_AUX)
        term_aux_h = factor_ptr
        term_aux_s = self.term_aux(stop, term_aux_h)
        term_ptr = term_aux_s
        return term_ptr

    def term_aux(self, stop, term_aux_h):
        """4. Term_aux ::= ASTERISK
                        Factor
                        {Term_aux1.h := mknode(ASTERISK, Term_aux.h, Factor.ptr)}
                        Term_aux1
                        {Term_aux.s := Term_aux1.s} |
                           DIVIDE
                        Factor
                        {Term_aux1.h := mknode(DIVIDE, Term_aux.h, Factor.ptr)}
                        Term_aux1
                        {Term_aux.s := Term_aux1.s} |
                           EPSILON
                           {Term_aux.s := Term_aux.h}"""
        term_aux_s = None
        if (self.lookahead[0] == C.t_ASTERISK):
            self.match(C.t_ASTERISK, stop | C.first_FACTOR | C.first_TERM_AUX)
            factor_ptr = self.factor(stop | C.first_TERM_AUX)
            term_aux1_h = node.Nodo(C.c_ASTERISK, term_aux_h, factor_ptr)
            term_aux1_s = self.term_aux(stop, term_aux1_h)
            term_aux_s = term_aux1_s
        elif (self.lookahead[0] == C.t_DIVIDE):
            self.match(C.t_DIVIDE, stop | C.first_FACTOR | C.first_TERM_AUX)
            factor_ptr = self.factor(stop | C.first_TERM_AUX)
            term_aux1_h = node.Nodo(C.c_DIVIDE, term_aux_h, factor_ptr)
            term_aux1_s = self.term_aux(stop, term_aux1_h)
            term_aux_s = term_aux1_s
        else:
            term_aux_s = term_aux_h
            self.syntax_check(stop)
        return term_aux_s

    def factor(self, stop):
        """5. Factor ::= LEFTPARENTHESIS Expr RIGHTPARENTHESIS
                        {Factor.ptr := Expr.ptr} |
                       MINUS Factor1
                      {Factor.ptr := mkunode(MINUS, Factor1.ptr)} |
                       ID {Factor.ptr := mkleaf(ID, ID.ptr)} |
                      NUM {Factor.ptr := mkleaf(NUM, NUM.ptr)} |
                      FLOAT {Factor.ptr := mkleaf(FLOAT, FLOAT.ptr)}"""
        factor_ptr = None
        if (self.lookahead[0] == C.t_LEFTPARENTHESIS):
            self.match(C.t_LEFTPARENTHESIS, stop |
                       C.first_EXPR | set([C.t_RIGHTPARENTHESIS]))
            expr_ptr = self.expr(stop | set([C.t_RIGHTPARENTHESIS]))
            self.match(C.t_RIGHTPARENTHESIS, stop)
            factor_ptr = expr_ptr
        elif (self.lookahead[0] == C.t_MINUS):
            self.match(C.t_MINUS, stop | C.first_FACTOR)
            factor1_ptr = self.factor(stop)
            factor_ptr = node.Nodo(C.c_MINUS, None, factor1_ptr)
        elif (self.lookahead[0] == C.t_ID):
            factor_ptr = node.Nodo(self.lookahead[1])
            self.match(C.t_ID, stop)
        elif (self.lookahead[0] == C.t_NUM):
            factor_ptr = node.Nodo(self.lookahead[1])
            self.match(C.t_NUM, stop)
        elif (self.lookahead[0] == C.t_FLOAT):
            factor_ptr = node.Nodo(self.lookahead[1])
            self.match(C.t_FLOAT, stop)
        else:
            self.syntax_error(stop)
        return factor_ptr

#!/usr/bin/python
#  -*-  coding: utf-8 -*-
""" Modulo que implementa el analizador lexico
    Autores: Ricardo Urdin Mata, Ruben Cabrera, Manuel Luis"""

from sources.constant.Constants import Const as C


class Lex:
    """Clase que implementa las funciones del analizador lexico"""

    def __init__(self, exp):
        """ Constructor: inicia las variables, y ralizara el analisis
            de la expresion que se le pasa como parametro"""
        self.fuente = exp
        self.tam = len(self.fuente)
        self.indice = 0
        self.tabla_de_simbolos = {}
        self.tabla_de_simbolos_index = 0
        self.num_linea = 1
        self.num_columna = 1
        self.num_linea_error = -1
        self.num_columna_error = -1
        self.token_atributo = []
        self.lexema = ""
        self.char = ''

    # funcion de avance de caracter
    #   1)  actualiza el contador de linea y de columna
    #   2)  avanza el indice de la fuente para examinar
    #       el siguiente caracter
    def avance(self):
        """ Actualiza contador de lineas y columnas y avanza
            hasta el siguiente caracter a analizar"""
        if (self.char == C.c_TAB):
            self.num_columna += C.TAB_SIZE
        elif (self.char == C.c_NEWLINE):
            self.num_columna = 1
            self.num_linea += 1
        else:
            self.num_columna += 1
        self.indice += 1

    def yylex(self):
        """ Funcion que devuelve el siguiente token que se
            encuentra en el fichero"""
        while (self.indice <= self.tam):

            self.token_atributo = []
            self.lexema = ""
            self.num_linea_error = self.num_linea
            self.num_columna_error = self.num_columna

            if (self.indice < self.tam):
                self.char = self.fuente[self.indice]
            else:
                self.token_atributo.append(C.t_ENDTEXT)
                return self.token_atributo

            # modo SIMBOLOS
            if (self.char in (
            C.c_PLUS, C.c_MINUS, C.c_ASTERISK, C.c_DIVIDE, C.c_RIGHTPARENTHESIS, C.c_LEFTPARENTHESIS)):
                self.avance()
                if (self.char == C.c_PLUS):
                    self.token_atributo.append(C.t_PLUS)
                elif (self.char == C.c_MINUS):
                    self.token_atributo.append(C.t_MINUS)
                elif (self.char == C.c_ASTERISK):
                    self.token_atributo.append(C.t_ASTERISK)
                elif (self.char == C.c_DIVIDE):
                    self.token_atributo.append(C.t_DIVIDE)
                elif (self.char == C.c_RIGHTPARENTHESIS):
                    self.token_atributo.append(C.t_RIGHTPARENTHESIS)
                else:
                    self.token_atributo.append(C.t_LEFTPARENTHESIS)
                self.token_atributo.append(self.char)
                return self.token_atributo

            # modo ID
            elif (self.char == C.c_C_LOWER):
                self.lexema += self.char
                self.avance()
                if (self.indice < self.tam):
                    self.char = self.fuente[self.indice]
                while ((self.char.isdigit()) and (self.indice < self.tam)):
                    self.lexema += self.char
                    self.avance()
                    if (self.indice < self.tam):
                        self.char = self.fuente[self.indice]
                # comprobacion de IDs en la tabla de simbolos
                #   si el ID no esta en la tabla de simbolos lo
                # insertamos y lo devolvemos
                #   si el ID esta en la tabla de simbolos no lo
                # insertamos y lo devolvemos
                if not (self.lexema.lower() in self.tabla_de_simbolos):
                    self.tabla_de_simbolos[self.lexema.lower()] = self.tabla_de_simbolos_index
                    self.tabla_de_simbolos_index += 1
                self.token_atributo.append(C.t_ID)
                self.token_atributo.append(self.lexema)
                return self.token_atributo

            # modo NUM o modo FLOAT
            elif (self.char.isdigit() or self.char == C.c_DOT):

                tokenType = 0

                while ((self.char.isdigit() or
                                self.char in (C.c_DOT, C.c_PLUS, C.c_MINUS, C.c_E_UPPER, C.c_E_LOWER))
                       and self.indice < self.tam):

                    if self.char in (C.c_DOT, C.c_PLUS, C.c_MINUS, C.c_E_UPPER, C.c_E_LOWER):
                        tokenType = 1

                    self.lexema += self.char
                    self.avance()
                    if (self.indice < self.tam):
                        self.char = self.fuente[self.indice]

                if tokenType == 0:  # entero --> t_NUM
                    self.token_atributo.append(C.t_NUM)
                    self.token_atributo.append(int(self.lexema))
                    return self.token_atributo
                else:  # flotante --> t_FLOAT
                    try:
                        value = float(self.lexema)
                        self.token_atributo.append(C.t_FLOAT)
                        self.token_atributo.append(float(self.lexema))
                    except ValueError, e:
                        self.token_atributo.append(C.t_TOKEN_ERROR)
                        self.token_atributo.append(C.ILLEGAL_FLOAT)
                        self.token_atributo.append(self.num_linea_error)
                        self.token_atributo.append(self.num_columna_error)
                    return self.token_atributo

            # modo SEPARADORES
            elif (self.char in (C.c_SPACE, C.c_NEWLINE, C.c_TAB, C.c_CARRIAGE_RETURN)):
                while (
                    (self.char in (C.c_SPACE, C.c_NEWLINE, C.c_TAB, C.c_CARRIAGE_RETURN)) and (self.indice < self.tam)):
                    self.avance()
                    if (self.indice < self.tam):
                        self.char = self.fuente[self.indice]

            # modo ILEGAL
            else:
                self.avance()
                self.token_atributo.append(C.t_TOKEN_ERROR)
                self.token_atributo.append(C.ILLEGAL_CHARACTER)
                self.token_atributo.append(self.num_linea_error)
                self.token_atributo.append(self.num_columna_error)
                return self.token_atributo

    def analizar_fichero(self):
        """Funcion que analiza los tokens que hay en el fichero"""
        self.indice = 0
        self.num_linea = 1
        self.num_columna = 1
        tok = self.yylex()
        print "\nLista de TOKENS:\n"
        print "Linea\t\tColumna\t\tToken\t\tLexema\Descripcion"
        while (tok[0] != C.t_ENDTEXT):
            if (tok[0] == C.t_TOKEN_ERROR):
                self.ImprimeTokenError(tok)
            else:
                self.ImprimeToken(tok)
            tok = self.yylex()
        self.ImprimeTokenEndText(tok)
        print "\nContenido de la tabla de simbolos:\n"
        print "ID\t\tIndice"
        for iden, pos in self.tabla_de_simbolos.iteritems():
            print iden, "\t\t", pos
        print

    def ImprimeTokenError(self, tokenerror):
        """imprimir linea_error, columna_error, TOKEN_ERROR, Descripcion"""
        print("{0}\t\t{1}\t\t{2}\t\t{3}").format(tokenerror[2], tokenerror[3], tokenerror[0], tokenerror[1])

    def ImprimeTokenEndText(self, tokenendtext):
        """imprimir linea, columna, ENDTEXT, Descripcion"""
        print("{0}\t\t{1}\t\t{2}\t\t{3}").format(self.num_linea, self.num_columna, tokenendtext[0], C.FIN)

    def ImprimeToken(self, token):
        """imprimir linea, columna, TOKEN, Lexema"""
        print("{0}\t\t{1}\t\t{2}\t\t{3}").format(self.num_linea, self.num_columna, token[0], token[1])

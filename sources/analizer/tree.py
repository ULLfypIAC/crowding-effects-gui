#!/usr/bin/python
# -*- coding: utf-8 -*-
""" Modulo que implementa a la Clase Arbol"""


class Arbol():
    """Metodos de la Clase Arbol"""

    def __init__(self, NodoRaiz=None):
        """Constructor que incializa la raiz del arbol"""
        self.raiz = NodoRaiz

    def preorden(self, nodo):
        """Recorrido preorden del arbol"""
        if (nodo == None):
            return ''
        valor = str(nodo.valor)
        if nodo.izquierdo != None:
            valor = valor + ' ' + self.preorden(nodo.izquierdo)
        if nodo.derecho != None:
            valor = valor + ' ' + self.preorden(nodo.derecho)
        return str(valor)

    def inorden(self, nodo):
        """Recorrido inorden del arbol"""
        if (nodo == None):
            return ''
        valor = ''
        if nodo.izquierdo != None:
            valor = valor + self.inorden(nodo.izquierdo) + ' '
        valor = valor + str(nodo.valor)
        if nodo.derecho != None:
            valor = valor + ' ' + self.inorden(nodo.derecho)
        return str(valor)

    def postorden(self, nodo):
        """Recorrido postorden del arbol"""
        if (nodo == None):
            return ''
        valor = ''
        if nodo.izquierdo != None:
            valor = valor + str(self.postorden(nodo.izquierdo)) + ' '
        if nodo.derecho != None:
            valor = valor + str(self.postorden(nodo.derecho)) + ' '
        valor = valor + str(nodo.valor)
        return str(valor)

#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import re
import sys

import numpy
from PyQt4.QtCore import Qt, QCoreApplication, QSettings, QModelIndex, QRect, pyqtSlot
from PyQt4.QtGui import QColor, QComboBox, QTableWidgetItem, QAbstractItemView, QHeaderView, QMainWindow, \
    QApplication, QFileDialog, QStyleFactory

from sources.constant.ApplicationConstants import ApplicationConsts as C
from sources.functionality.histogram import Histogram
from sources.functionality.manage import ManageDiagram
from sources.functionality.plotting import Diagram, Bundle
from sources.functionality.save_parameters_file import saveParametersFile
from sources.legacy.plot_CMDs import plot_cmds, plot_cmds_label
from sources.legacy.table_count import strictly_increasing, age_histo, met_histo
from sources.resources.aboutThis import AboutThis
from sources.resources.adviceDialog import AdviceDialog
from sources.resources.crowd import Ui_mainWindow


class MainWindow(QMainWindow):
    def __init__(self, q_application):
        super(MainWindow, self).__init__(parent=None)
        self.__init_load()
        self.__init_create(q_application)
        self.__init_customize()
        self.__init_layouts()
        self.__init_connect()

    def __init_load(self):
        self.ui = Ui_mainWindow()
        self.ui.setupUi(self)
        self.about = AboutThis()

    def __init_create(self, q_application):
        self.q_application = q_application
        # Variables que almacenan las expresiones actuales
        #   X, Y  --> Diagrama Observado
        #   X, Y, Edad, Metalicidad --> Diagrama Modelo
        self.strT1ObsExpXaxisActual = ''
        self.strT1ObsExpYaxisActual = ''
        self.strT1ModExpXaxisActual = ''
        self.strT1ModExpYaxisActual = ''
        self.strT1ModExpAgeActual = ''
        self.strT1ModExpMetallicityActual = ''
        #
        self.dialog = AdviceDialog()
        # Almacenamiento de los datos cargados desde fichero
        self.observedDiagram = ManageDiagram()
        self.modelDiagram = ManageDiagram()
        # Diagramas
        self.diagramT2Obs = Diagram.create(self.ui.groupT2Observed)
        self.diagramT2Mod = Diagram.create(self.ui.groupT2Model)
        # Histogramas
        self.histogramT2Obs = Histogram(self.ui.groupDockHistObs)
        self.histogramT2Mod = Histogram(self.ui.groupDockHistMod)
        # Bundle activo
        self.dictT2BundleSelected = {'instance': None, 'found': False, 'index': -1, 'row': -1}
        # Parametros para generacion de script
        self.listT2rangeX = None # lista para el rango eje X
        self.listT2rangeY = None # lista para el rango eje Y
        self.listT3AgeRanges = None
        self.listT3MetallicityRanges = None
        self.boolT3RefineGrid = None
        self.boolT3RefineAge = None
        self.strT3IntMother = None
        self.strT3AreaReg = None
        self.strT3BinningOffsetsX = None
        self.strT3BinningOffsetsY = None
        self.strT3DistRedd = None
        self.strT3DistReddCenterX = None
        self.strT3DistReddCenterY = None
        self.intT3NumberOfBins = None
        self.floatT3ZMin = None
        self.floatT3ZMax = None
        self.floatT3ZSun = None
        self.strT3NumberIterations = None
        self.strT3NumberProcessors = None
        # Colores para los QLineEdit de las expresiones
        self.strAllEmptyLineEdit = self.ui.lineT1ObsFile.styleSheet()
        self.strAllCorrectLineEdit = 'QLineEdit{background:rgba(0,255,0,192);}'
        self.strAllIncorrectLineEdit = 'QLineEdit{background:rgba(255,0,0,192);}'

    def __init_customize(self):
        # la pestaña 1 y 3 siempre estaran habilitadas
        self.ui.tabWidget.setTabEnabled(0, True)
        self.ui.tabWidget.setTabEnabled(2, True)
        # inicialmente la pestaña 3 estara deshabilitada
        self.ui.tabWidget.setTabEnabled(1, False)
        # Haciendo focus sobre la pestaña 1
        self.ui.tabWidget.setCurrentIndex(0)
        self.ui.tab1.setFocus()
        # Desactivando las lineas de texto donde se muestran los nombres de los ficheros
        self.ui.lineT1ObsFile.setDisabled(True)
        self.ui.lineT1ModFile.setDisabled(True)
        # Desactivando las lineas de texto donde se cargan las expresiones de las columnas
        self.ui.lineT1ObsXaxis.setDisabled(True)
        self.ui.lineT1ObsYaxis.setDisabled(True)
        self.ui.lineT1ModXaxis.setDisabled(True)
        self.ui.lineT1ModYaxis.setDisabled(True)
        self.ui.lineT1ModAge.setDisabled(True)
        self.ui.lineT1ModMetallicity.setDisabled(True)
        # Desactivando las lineas de texto para los rotulos de los plots
        self.ui.lineT1ObsLabXaxis.setDisabled(True)
        self.ui.lineT1ObsLabYaxis.setDisabled(True)
        self.ui.lineT1ModLabXaxis.setDisabled(True)
        self.ui.lineT1ModLabYaxis.setDisabled(True)
        # Configurar estados de los diagramas
        self.diagramT2Obs.canvas.setEnabled(True)
        self.diagramT2Mod.canvas.setEnabled(True)
        self.diagramT2Obs.toolbar.setEnabled(True)
        self.diagramT2Mod.toolbar.setEnabled(True)
        # Deshabilitar las QAccions y QDockWidgets de la tab2
        # Desactivar Diagrama Tools
        self.ui.dockT2DiagramTools.setVisible(False)
        self.ui.actionDiagram.setChecked(False)
        self.ui.actionDiagram.setDisabled(True)
        # Desactivar BundleTools
        self.ui.dockT2BundleTools.setVisible(False)
        self.ui.actionBundle.setChecked(False)
        self.ui.actionBundle.setDisabled(True)
        # Desactivar HistogramTools
        self.ui.dockT2HistogramTools.setVisible(False)
        self.ui.actionHistogram.setChecked(False)
        self.ui.actionHistogram.setDisabled(True)
        #
        self.clearAgeMetallicityElements()
        # Configurar Diagramas
        self.diagramT2Obs.canvas.setCursor(Qt.CrossCursor)
        self.diagramT2Mod.canvas.setCursor(Qt.CrossCursor)
        # Configurar SpinBox AddPoint
        self.ui.spinT2PointX.setDecimals(C.NUM_DECIMAL_DIGITS_POINTS)
        self.ui.spinT2PointX.setSingleStep(10 ** -C.NUM_DECIMAL_DIGITS_POINTS)
        self.ui.spinT2PointY.setDecimals(C.NUM_DECIMAL_DIGITS_POINTS)
        self.ui.spinT2PointY.setSingleStep(10 ** -C.NUM_DECIMAL_DIGITS_POINTS)
        # Configurar SpinBox Bundle Bin Size Plot
        self.ui.spinT2BinSizeX.setDecimals(C.NUM_DECIMAL_DIGITS_BUNDLES)
        self.ui.spinT2BinSizeX.setSingleStep(10 ** -C.NUM_DECIMAL_DIGITS_BUNDLES)
        self.ui.spinT2BinSizeY.setDecimals(C.NUM_DECIMAL_DIGITS_BUNDLES)
        self.ui.spinT2BinSizeY.setSingleStep(10 ** -C.NUM_DECIMAL_DIGITS_BUNDLES)
        # Configurar SpinBox Bundle Bin Size
        self.ui.spinT2BundleBinSizeX.setDecimals(C.NUM_DECIMAL_DIGITS_BUNDLES)
        self.ui.spinT2BundleBinSizeX.setSingleStep(10 ** -C.NUM_DECIMAL_DIGITS_BUNDLES)
        self.ui.spinT2BundleBinSizeY.setDecimals(C.NUM_DECIMAL_DIGITS_BUNDLES)
        self.ui.spinT2BundleBinSizeY.setSingleStep(10 ** -C.NUM_DECIMAL_DIGITS_BUNDLES)
        # Configurar ComboBox
        self.ui.comboT2Bundle.setInsertPolicy(QComboBox.NoInsert)
        self.ui.comboT2Bundle.setEditable(True)
        self.ui.comboT2Bundle.lineEdit().setPlaceholderText('Select a bundle')
        # Configurar TableWidget de los bundles
        self.ui.tableT2Bundle.setColumnCount(2)
        self.ui.tableT2Bundle.setHorizontalHeaderLabels(['X', 'Y'])
        h_header_view = self.ui.tableT2Bundle.horizontalHeader()
        h_header_view.setResizeMode(QHeaderView.Stretch)
        del h_header_view
        self.ui.tableT2Bundle.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.ui.tableT2Bundle.setSelectionMode(QAbstractItemView.SingleSelection)
        self.ui.tableT2Bundle.setAlternatingRowColors(True)
        # Configurando las propiedades de la tabla de edad
        self.ui.tableT3Age.setColumnCount(3)
        self.ui.tableT3Age.setHorizontalHeaderLabels(['Lower', 'Upper', 'Count'])
        h_header_view = self.ui.tableT3Age.horizontalHeader()
        h_header_view.setResizeMode(QHeaderView.Stretch)
        del h_header_view
        # Configurando las propiedades de la tabla de metalicidad
        self.ui.tableT3Metallicity.setColumnCount(3)
        self.ui.tableT3Metallicity.setHorizontalHeaderLabels(['Lower', 'Upper', 'Count'])
        h_header_view = self.ui.tableT3Metallicity.horizontalHeader()
        h_header_view.setResizeMode(QHeaderView.Stretch)
        del h_header_view

    def __init_layouts(self):
        # Agregar los canvas y las toolbar de los diagramas a sus respectivos layouts
        self.ui.vLayoutT2groupObs.addWidget(self.diagramT2Obs.toolbar)
        self.ui.vLayoutT2groupObs.addWidget(self.diagramT2Obs.canvas)
        self.ui.vLayoutT2groupMod.addWidget(self.diagramT2Mod.toolbar)
        self.ui.vLayoutT2groupMod.addWidget(self.diagramT2Mod.canvas)
        # Agregar los canvas y las toolbar de los histogramas a sus respectivos layouts
        self.ui.gLayoutDockHistObs.addWidget(self.histogramT2Obs.toolbar, 0, 0)
        self.ui.gLayoutDockHistObs.addWidget(self.histogramT2Obs.canvas, 1, 0)
        self.ui.gLayoutDockHistMod.addWidget(self.histogramT2Mod.toolbar, 0, 0)
        self.ui.gLayoutDockHistMod.addWidget(self.histogramT2Mod.canvas, 1, 0)

    def __init_connect(self):

        def __init_connect_tab1(self):
            # Conectando los botones 'Load Observed File Diagram' y 'Load Model File Diagram'
            self.ui.buttonT1ObsOpen.clicked.connect(self.loadObservedDiagram)
            self.ui.buttonT1ModOpen.clicked.connect(self.loadModelDiagram)
            # Conectando las lineas de texto de las columnas (Diagrama Observado)
            self.ui.lineT1ObsXaxis.editingFinished.connect(self.loadObservedDiagramXaxis)
            self.ui.lineT1ObsYaxis.editingFinished.connect(self.loadObservedDiagramYaxis)
            # Conectando las lineas de texto de las columnas (Diagrama Modelo)
            self.ui.lineT1ModXaxis.editingFinished.connect(self.loadModelDiagramXaxis)
            self.ui.lineT1ModYaxis.editingFinished.connect(self.loadModelDiagramYaxis)
            self.ui.lineT1ModAge.editingFinished.connect(self.loadModelDiagramAge)
            self.ui.lineT1ModMetallicity.editingFinished.connect(self.loadModelDiagramMetallicity)
            # Conectando las lineas de texto de los titulos de los plots
            self.ui.lineT1ObsLabXaxis.editingFinished.connect(self.callingPlotCMDsLabel)
            self.ui.lineT1ObsLabYaxis.editingFinished.connect(self.callingPlotCMDsLabel)
            self.ui.lineT1ModLabXaxis.editingFinished.connect(self.callingPlotCMDsLabel)
            self.ui.lineT1ModLabYaxis.editingFinished.connect(self.callingPlotCMDsLabel)

        def __init_connect_bundle(self):
            # Conectando las elementos para manipulación de bundles
            self.diagramT2Obs.canvasPress.connect(self.diagram_events)
            self.diagramT2Mod.canvasPress.connect(self.diagram_events)
            self.ui.buttonT2BundleCreate.clicked.connect(self.bundle_create)
            self.ui.buttonT2BundleOpen.clicked.connect(self.bundle_open)
            self.ui.buttonT2BundleClose.clicked.connect(self.bundle_close)
            self.ui.buttonT2BundleRemove.clicked.connect(self.bundle_remove)
            self.ui.comboT2Bundle.currentIndexChanged.connect(self.bundle_selected)
            self.ui.tableT2Bundle.activated.connect(self.point_table_selected)
            self.ui.tableT2Bundle.clicked.connect(self.point_table_selected)
            self.ui.buttonT2PointCreate.clicked.connect(self.point_add)
            self.ui.buttonT2PointModify.clicked.connect(self.point_modify)
            self.ui.buttonT2PointRemove.clicked.connect(self.point_remove)
            self.ui.spinT2BundleBinSizeX.valueChanged.connect(self.saveBundleBinSizeX)
            self.ui.spinT2BundleBinSizeY.valueChanged.connect(self.saveBundleBinSizeY)
            # Conectando el boton para mostrar los histogramas de los bundles cerrados
            self.ui.buttonDockHistDraw.clicked.connect(self.buildHistograms)

        def __init_connect_diagrams(self):
            # Conectando los elementos para manipulación de los diagramas
            self.ui.lineT2MinX.editingFinished.connect(self.callingPlotCMDs)
            self.ui.lineT2MaxX.editingFinished.connect(self.callingPlotCMDs)
            self.ui.lineT2MinY.editingFinished.connect(self.callingPlotCMDs)
            self.ui.lineT2MaxY.editingFinished.connect(self.callingPlotCMDs)
            #
            self.ui.checkboxT2colorbar.stateChanged.connect(self.callingPlotCMDs)
            #
            self.ui.comboT2Scale.currentIndexChanged.connect(self.callingPlotCMDs)
            self.ui.spinT2BinSizeX.valueChanged.connect(self.callingPlotCMDs)
            self.ui.spinT2BinSizeY.valueChanged.connect(self.callingPlotCMDs)

        def __init_connect_tab3(self):
            # Conectando los lines edit cuyas entradas generaran los rangos de metalicidad
            self.ui.lineT3NumberOfBins.editingFinished.connect(self.loadNumberOfBins)
            self.ui.lineT3ZMin.editingFinished.connect(self.loadZMin)
            self.ui.lineT3ZMax.editingFinished.connect(self.loadZMax)
            self.ui.lineT3ZSun.editingFinished.connect(self.loadZSun)
            # Conectando los lines edit donde se escriben los rangos de edad y metalicidad
            self.ui.lineT3AgeRanges.editingFinished.connect(self.loadAgeRanges)
            self.ui.lineT3MetallicityRanges.editingFinished.connect(self.loadMetallicityRanges)
            # Conectando los lines edit de los elementos que no se usan en la APP
            self.ui.checkboxT3RefineGrid.stateChanged.connect(self.setStateRefineGrid)
            self.ui.checkboxT3RefineAge.stateChanged.connect(self.setStateRefineAge)
            self.ui.lineT3IntMother.editingFinished.connect(self.loadIntMother)
            self.ui.lineT3AreaReg.editingFinished.connect(self.loadAreaReg)
            self.ui.lineT3BinningOffsetsX.editingFinished.connect(self.loadBinningOffsetsX)
            self.ui.lineT3BinningOffsetsY.editingFinished.connect(self.loadBinningOffsetsY)
            self.ui.lineT3DistRedd.editingFinished.connect(self.loadDistRedd)
            self.ui.lineT3DistReddCenterX.editingFinished.connect(self.loadDistReddCenterX)
            self.ui.lineT3DistReddCenterY.editingFinished.connect(self.loadDistReddCenterY)
            self.ui.lineT3NumberIterations.editingFinished.connect(self.loadNumberIterations)
            self.ui.lineT3NumberProcessors.editingFinished.connect(self.loadNumberProcessors)

        def __init_connect_actions(self):
            # Conectando QAccion para cargar el fichero de parametros
            self.ui.actionOpenParametersFile.triggered.connect(self.open_parameters_file)
            # Conectando QAccion para salvar el fichero de parametros
            self.ui.actionSaveParametersFile.triggered.connect(self.save_parameters_file)
            # Conectando QAccion para cargar un estado de aplicacion desde fichero
            self.ui.actionOpenSettings.triggered.connect(self.load_settings)
            # Conectando QAccion para salvar el estado actual de la aplicacion a fichero
            self.ui.actionSaveSettings.triggered.connect(self.save_settings)
            #

        def __init_others(self):
            self.ui.tabWidget.currentChanged.connect(self.activateTools)
            self.ui.actionAbout.triggered.connect(self.about_this)
            self.ui.actionAboutQt.triggered.connect(self.about_qt)

        __init_connect_tab1(self)
        __init_connect_bundle(self)
        __init_connect_diagrams(self)
        __init_connect_tab3(self)
        __init_connect_actions(self)
        __init_others(self)

    def resetAppState(self):
        # reiniciar valores de manejo de ficheros
        self.observedDiagram.clearContents()
        self.modelDiagram.clearContents()
        # reiniciar valores de expresiones
        self.strT1ObsExpXaxisActual = ''
        self.strT1ObsExpYaxisActual = ''
        self.strT1ModExpXaxisActual = ''
        self.strT1ModExpYaxisActual = ''
        self.strT1ModExpAgeActual = ''
        self.strT1ModExpMetallicityActual = ''
        # reiniciar variables elementos pestania 2
        self.listT2rangeX = None
        self.listT2rangeY = None
        # reiniciar variables elementos pestania 3
        self.listT3AgeRanges = None
        self.listT3MetallicityRanges = None
        self.boolT3RefineGrid = None
        self.boolT3RefineAge = None
        self.strT3IntMother = None
        self.strT3AreaReg = None
        self.strT3BinningOffsetsX = None
        self.strT3BinningOffsetsY = None
        self.strT3DistRedd = None
        self.strT3DistReddCenterX = None
        self.strT3DistReddCenterY = None
        self.intT3NumberOfBins = None
        self.floatT3ZMin = None
        self.floatT3ZMax = None
        self.floatT3ZSun = None
        self.strT3NumberIterations = None
        self.strT3NumberProcessors = None
        # reiniciar elementos aplicacion
        self.clearObservedLinesEdits()
        self.clearModelLinesEdits()
        self.clearTab2()
        self.clearAgeMetallicityElements()
        self.clearOthers()

    @pyqtSlot()
    def open_parameters_file(self):
        pathToFile = QFileDialog.getOpenFileName(
                parent=self,
                caption=QCoreApplication.translate('MainWindow', 'Open parameters file'),
                directory='.',
                filter=QCoreApplication.translate('MainWindow', 'IN files (*.in)')
        )
        stdPythonPathToFile = str(pathToFile)
        if len(stdPythonPathToFile) > 0:

            self.resetAppState()

            # reading parameters file keys
            parametersFile = open(stdPythonPathToFile)
            data = parametersFile.readlines()
            file_in = {}
            for line in data:
                # ignore empty lines and comments (linux y windows)
                if ((line[0] == '#') or (line[0] == '\n') or (line[0] == '\r')):
                    continue
                # parse input, assign values to variables
                key, value = line.split("=")
                if key.strip() in file_in:
                    file_in[key.strip()] = file_in[key.strip()] + '/' + value.strip()
                else:
                    file_in[key.strip()] = value.strip()
            parametersFile.close()

            # Observed CMD
            if len(filter(None, re.split("[']+", file_in['obscmd']))) > 0:
                obscmd = filter(None, re.split("[']+", file_in['obscmd']))[0]
                if obscmd != "None":
                    self.loadObservedDiagram(obscmd)

            obsExpXaxis = file_in['obsExpXaxis']
            self.loadObservedDiagramXaxis(obsExpXaxis)

            obsExpYaxis = file_in['obsExpYaxis']
            self.loadObservedDiagramYaxis(obsExpYaxis)

            # Model CMD
            if len(filter(None, re.split("[']+", file_in['modcmd']))) > 0:
                modcmd = filter(None, re.split("[']+", file_in['modcmd']))[0]
                if modcmd != "None":
                    self.loadModelDiagram(modcmd)

            modExpXaxis = file_in['modExpXaxis']
            self.loadModelDiagramXaxis(modExpXaxis)

            modExpYaxis = file_in['modExpYaxis']
            self.loadModelDiagramYaxis(modExpYaxis)

            modExpAge = file_in['modExpAge']
            self.loadModelDiagramAge(modExpAge)

            modExpMetallicity = file_in['modExpMetallicity']
            self.loadModelDiagramMetallicity(modExpMetallicity)

            # CMD color and magnitude ranges
            ran_x_s = filter(None, re.split("[, \[\]]+", file_in['ran_c']))
            ran_x = None
            if len(ran_x_s) > 1:
                ran_x = [float(i) for i in ran_x_s]

            ran_y_s = filter(None, re.split("[, \[\]]+", file_in['ran_m']))
            ran_y = None
            if len(ran_y_s) > 1:
                ran_y = [float(i) for i in ran_y_s]

            # Bundles
            bundles = []
            if 'b' in file_in:
                b = filter(None, re.split("[/\[\]]+", file_in['b']))
                b = [bundle for bundle in b if len(bundle) > 2]

                for i in range(len(b) / 2):
                    bun = [[float(j) for j in filter(None, re.split("[, \[\]]+", b[2 * i]))],
                           [float(j) for j in filter(None, re.split("[, \[\]]+", b[2 * i + 1]))]]
                    bundles += [numpy.array(bun).T]

                i = 0;
                bundles_aux = []
                for bund in file_in['b'].split("/"):
                    if bund == "[[], []]":
                        bundles_aux.append(numpy.array([]))
                    else:
                        bundles_aux.append(bundles[i])
                        i += 1
                bundles = bundles_aux

            # Bin sizes in color and magnitude
            bin_xy = []
            if 'bin_cm' in file_in:
                binxy = filter(None, re.split("[/\[\]]+", file_in['bin_cm']))

                for i in range(len(binxy)):
                    bb = [float(j) for j in filter(None, re.split("[, \[\]]+", binxy[i]))]
                    bin_xy += [numpy.array(bb).T]
                bin_xy = numpy.array(bin_xy)

            if self.ui.tab2.isEnabled() == True:
                self.callingPlotCMDs(   parameters_xRange=ran_x,
                                        parameters_yRange=ran_y)
                for bundle, bin_size in zip(bundles, bin_xy):
                    timesToCallDiagramEvents = len(bundle)
                    bundleClose = False
                    if (len(bundle) > 3) and (bundle[0][0] == bundle[-1][0] and bundle[0][1] == bundle[-1][1]):
                        timesToCallDiagramEvents -= 1
                        bundleClose = True
                    self.bundle_create()
                    for i in range(timesToCallDiagramEvents):
                        self.diagram_events({'button': 1, 'xdata': bundle[i][0], 'ydata': bundle[i][1]})
                    if bundleClose == True:
                        self.bundle_close()
                    self.saveBundleBinSizeX(bin_size[0])
                    self.ui.spinT2BundleBinSizeX.setValue(bin_size[0])
                    self.saveBundleBinSizeY(bin_size[1])
                    self.ui.spinT2BundleBinSizeY.setValue(bin_size[1])

            # Age (in Gyr) and metallicity (in Z) bins
            if file_in['age'] == "None":
                file_in['age'] = "[0., 0.25, 0.5, 0.75, 1., 1.25, 1.5, 1.75, 2., 2.5, 3., 3.5, 4., 4.5, 5., 6., 7., 8., 9., 10., 11., 12., 13., 14., 15.]"
            age_s = filter(None, re.split("[, \[\]]+", file_in['age']))
            if self.ui.lineT3AgeRanges.isEnabled() == True:
                self.listT3AgeRanges = [float(i) for i in age_s]
                self.ui.lineT3AgeRanges.clear()
                self.ui.lineT3AgeRanges.setText(str(self.listT3AgeRanges).strip('[]'))
                self.ui.lineT3AgeRanges.editingFinished.emit()

            if file_in['met'] == "None":
                file_in['met'] = "[0.0004, 0.0007, 0.0011, 0.0016, 0.0022, 0.0029, 0.0037, 0.0046, 0.0056, 0.0067, 0.0079, 0.0092, 0.0106, 0.0122, 0.0140, 0.0160, 0.0182, 0.0206, 0.0232, 0.0260, 0.0290]"
            met_s = filter(None, re.split("[, \[\]]+", file_in['met']))
            if self.ui.lineT3MetallicityRanges.isEnabled() == True:
                self.listT3MetallicityRanges = [float(i) for i in met_s]
                self.ui.lineT3MetallicityRanges.clear()
                self.ui.lineT3MetallicityRanges.setText(str(self.listT3MetallicityRanges).strip('[]'))
                self.ui.lineT3MetallicityRanges.editingFinished.emit()

            # Parameters to calculate met bins
            if file_in['number_of_bins'] == "None":
                file_in['number_of_bins'] = "12"
            self.ui.lineT3NumberOfBins.setText(file_in['number_of_bins'])
            self.intT3NumberOfBins = int(file_in['number_of_bins'])

            if file_in['z_min'] == "None":
                file_in['z_min'] = "0.0001"
            self.ui.lineT3ZMin.setText(file_in['z_min'])
            self.floatT3ZMin = float(file_in['z_min'])

            if file_in['z_max'] == "None":
                file_in['z_max'] = "0.002"
            self.ui.lineT3ZMax.setText(file_in['z_max'])
            self.floatT3ZMax = float(file_in['z_max'])

            if file_in['z_sun'] == "None":
                file_in['z_sun'] = "0.0198"
            self.ui.lineT3ZSun.setText(file_in['z_sun'])
            self.floatT3ZSun = float(file_in['z_sun'])

            # Normalizing constants (intmother in M_sun; areareg in pc^2)
            if file_in['intmother'] == "None":
                file_in['intmother'] = "1."
            self.ui.lineT3IntMother.setText(file_in['intmother'])
            self.ui.lineT3IntMother.editingFinished.emit()
            if file_in['areareg'] == "None":
                file_in['areareg'] = "1."
            self.ui.lineT3AreaReg.setText(file_in['areareg'])
            self.ui.lineT3AreaReg.editingFinished.emit()

            # Offset observed CMD in color & magnitude (i.e. ~ reddening & distance)...
            # (S)earch minimum, check only (O)ne position, or sample full (G)rid?
            if file_in['dist_redd'] == "'None'":
                file_in['dist_redd'] = "'S'"
            self.ui.lineT3DistRedd.setText(file_in['dist_redd'].strip('\''))
            self.ui.lineT3DistRedd.editingFinished.emit()

            # ...centered on:
            if file_in['dist_redd_center'] == "[None, None]":
                file_in['dist_redd_center'] = "[0.0, 0.0]"
            self.ui.lineT3DistReddCenterX.setText(file_in['dist_redd_center'].strip('[]').split(", ")[0])
            self.ui.lineT3DistReddCenterX.editingFinished.emit()

            self.ui.lineT3DistReddCenterY.setText(file_in['dist_redd_center'].strip('[]').split(", ")[1])
            self.ui.lineT3DistReddCenterY.editingFinished.emit()

            # Refine the grid to find the minimum in the chi2 map?
            self.ui.checkboxT3RefineGrid.setChecked(eval(file_in['refine_grid']))

            # Number of binning offsets in color and magnitude
            if file_in['shifts'] == "(None, None)":
                file_in['shifts'] = "(2, 2)"
            self.ui.lineT3BinningOffsetsX.setText(file_in['shifts'].strip('()').split(", ")[0])
            self.ui.lineT3BinningOffsetsX.editingFinished.emit()

            self.ui.lineT3BinningOffsetsY.setText(file_in['shifts'].strip('()').split(", ")[1])
            self.ui.lineT3BinningOffsetsY.editingFinished.emit()

            # Also shift the binning in age?
            self.ui.checkboxT3RefineAge.setChecked(eval(file_in['refine_age']))

            # Number of iterations of Poissonian resampling of the observed CMD
            if file_in['nPoisson'] == "None":
                file_in['nPoisson'] = "4"
            self.ui.lineT3NumberIterations.setText(file_in['nPoisson'])
            self.ui.lineT3NumberIterations.editingFinished.emit()

            # Number of processors to use when running in parallel
            if file_in['nProcessors'] == "None":
                file_in['nProcessors'] = "mp.cpu_count()"
            self.ui.lineT3NumberProcessors.setText(file_in['nProcessors'])
            self.ui.lineT3NumberProcessors.editingFinished.emit()

    @pyqtSlot()
    def save_parameters_file(self):
        pathToFile = QFileDialog.getSaveFileName(
                parent=self,
                caption=QCoreApplication.translate('MainWindow', 'Save parameters file'),
                directory='.',
                filter=QCoreApplication.translate('MainWindow', 'IN files (*.in)')
        )
        stdPythonPathToFile = str(pathToFile)
        if len(stdPythonPathToFile) > 0:
            saveParametersFile(pathToFile,
                               self.observedDiagram.stdPythonPathToFile,
                               self.strT1ObsExpXaxisActual,
                               self.strT1ObsExpYaxisActual,
                               self.modelDiagram.stdPythonPathToFile,
                               self.strT1ModExpXaxisActual,
                               self.strT1ModExpYaxisActual,
                               self.strT1ModExpAgeActual,
                               self.strT1ModExpMetallicityActual,
                               self.listT2rangeX,
                               self.listT2rangeY,
                               Bundle.instances,
                               self.listT3AgeRanges,
                               self.listT3MetallicityRanges,
                               self.intT3NumberOfBins,
                               self.floatT3ZMin,
                               self.floatT3ZMax,
                               self.floatT3ZSun,
                               self.strT3IntMother,
                               self.strT3AreaReg,
                               self.strT3DistRedd,
                               self.strT3DistReddCenterX,
                               self.strT3DistReddCenterY,
                               self.boolT3RefineGrid,
                               self.strT3BinningOffsetsX,
                               self.strT3BinningOffsetsY,
                               self.boolT3RefineAge,
                               self.strT3NumberIterations,
                               self.strT3NumberProcessors)

    @pyqtSlot()
    def load_settings(self):
        def load_windows(setting):
            # >> Windows
            setting.beginGroup('Windows')
            value = setting.value('geometry', None, QRect)
            self.setGeometry(value)
            # << Windows
            setting.endGroup()

        def load_observed(setting):
            # >> Observed diagram
            setting.beginGroup('Observed')
            # Observed file
            value = setting.value('obsFile', None, str)
            self.observedDiagram.stdPythonPathToFile = str(value)
            if os.path.isfile(self.observedDiagram.stdPythonPathToFile):
                self.loadObservedDiagram(self.observedDiagram.stdPythonPathToFile)
                # Observed X-axis data expression
                value = setting.value('obsExpXaxis', None, str)
                self.strT1ObsExpXaxisActual = ''
                self.ui.lineT1ObsXaxis.setText(str(value))
                self.ui.lineT1ObsXaxis.editingFinished.emit()
                # Observed Y-axis data expression
                value = setting.value('obsExpYaxis', None, str)
                self.strT1ObsExpYaxisActual = ''
                self.ui.lineT1ObsYaxis.setText(str(value))
                self.ui.lineT1ObsYaxis.editingFinished.emit()
                # Observed X-axis label
                value = setting.value('obsLabXaxis', None, str)
                self.ui.lineT1ObsLabXaxis.setText(str(value))
                self.ui.lineT1ObsLabXaxis.editingFinished.emit()
                # Observed Y-axis label
                value = setting.value('obsLabYaxis', None, str)
                self.ui.lineT1ObsLabYaxis.setText(str(value))
                self.ui.lineT1ObsLabYaxis.editingFinished.emit()
            else:
                self.dialog.customizeLabelErrorMessage(
                        QCoreApplication.translate(
                                'MainWindow',
                                'Observed path file not is an existing regular file:\n' +
                                self.observedDiagram.stdPythonPathToFile
                        )
                )
                self.dialog.exec_()
            # << Observed diagram
            setting.endGroup()

        def load_model(setting):
            # >> Model diagram
            setting.beginGroup('model')
            # Model file
            value = setting.value('modFile', None, str)
            self.modelDiagram.stdPythonPathToFile = str(value)
            if os.path.isfile(self.modelDiagram.stdPythonPathToFile):
                self.loadModelDiagram(self.modelDiagram.stdPythonPathToFile)
                # Model X-axis data expression
                value = setting.value('modExpXaxis', None, str)
                self.strT1ModExpXaxisActual = ''
                self.ui.lineT1ModXaxis.setText(str(value))
                self.ui.lineT1ModXaxis.editingFinished.emit()
                # Model Y-axis data expression
                value = setting.value('modExpYaxis', None, str)
                self.strT1ModExpYaxisActual = ''
                self.ui.lineT1ModYaxis.setText(str(value))
                self.ui.lineT1ModYaxis.editingFinished.emit()
                # Model Age data expression
                value = setting.value('modExpAge', None, str)
                self.strT1ModExpAgeActual = ''
                self.ui.lineT1ModAge.setText(str(value))
                self.ui.lineT1ModAge.editingFinished.emit()
                # Model Metallicity data expression
                value = setting.value('modExpMetal', None, str)
                self.strT1ModExpMetallicityActual = ''
                self.ui.lineT1ModMetallicity.setText(str(value))
                self.ui.lineT1ModMetallicity.editingFinished.emit()
                # Model X-axis label
                value = setting.value('modLabXaxis', None, str)
                self.ui.lineT1ModLabXaxis.setText(str(value))
                self.ui.lineT1ModLabXaxis.editingFinished.emit()
                # Model Y-axis label
                value = setting.value('modLabYaxis', None, str)
                self.ui.lineT1ModLabYaxis.setText(str(value))
                self.ui.lineT1ModLabYaxis.editingFinished.emit()
            else:
                self.dialog.customizeLabelErrorMessage(
                        QCoreApplication.translate(
                                'MainWindow',
                                'Model path file not is an existing regular file:\n' +
                                self.modelDiagram.stdPythonPathToFile
                        )
                )
                self.dialog.exec_()
            # << Model diagram
            setting.endGroup()

        def load_bundles(setting):
            if self.ui.tab2.isEnabled():
                # >> Bundles
                setting.beginGroup('Bundles')
                # >> bundle
                size_bundle = setting.beginReadArray('bundle')
                for index_bundle in range(size_bundle):
                    setting.setArrayIndex(index_bundle)
                    self.bundle_create()
                    # >> bundle.points
                    size_point = setting.beginReadArray('point')
                    for index_point in range(size_point):
                        setting.setArrayIndex(index_point)
                        xdata = setting.value('X', None, float)
                        ydata = setting.value('Y', None, float)
                        self.diagram_events({
                            'button': 1,
                            'xdata': xdata,
                            'ydata': ydata
                        })
                    # << bundle.points
                    setting.endArray()
                    value = setting.value('binSizeX', None, float)
                    self.ui.spinT2BundleBinSizeX.setValue(value)
                    value = setting.value('binSizeY', None, float)
                    self.ui.spinT2BundleBinSizeY.setValue(value)
                    value = setting.value('closed', None, bool)
                    if value:
                        self.bundle_close()
                # << bundle
                setting.endArray()
                # << Bundles
                setting.endGroup()

        def load_tables(setting):
            # >> Tables
            setting.beginGroup('Tables')
            value = setting.value('age', None, str)
            if self.ui.lineT3AgeRanges.isEnabled() == True:
                self.ui.lineT3AgeRanges.setText(str(value))
                self.ui.lineT3AgeRanges.editingFinished.emit()
            value = setting.value('metallicity', None, str)
            if self.ui.lineT3MetallicityRanges.isEnabled() == True:
                self.ui.lineT3MetallicityRanges.setText(str(value))
                self.ui.lineT3MetallicityRanges.editingFinished.emit()
            # << Tables
            setting.endGroup()

        def load_others(setting):
            # >> Others
            setting.beginGroup('Others')
            value = setting.value('refineGrid', None, bool)
            self.ui.checkboxT3RefineGrid.setChecked(value)
            value = setting.value('refineAge', None, bool)
            self.ui.checkboxT3RefineAge.setChecked(value)
            value = setting.value('intMother', None, str)
            self.ui.lineT3IntMother.setText(str(value))
            self.ui.lineT3IntMother.editingFinished.emit()
            value = setting.value('areaReg', None, str)
            self.ui.lineT3AreaReg.setText(str(value))
            self.ui.lineT3AreaReg.editingFinished.emit()
            value = setting.value('binningOffsetsX', None, str)
            self.ui.lineT3BinningOffsetsX.setText(str(value))
            self.ui.lineT3BinningOffsetsX.editingFinished.emit()
            value = setting.value('binningOffsetsY', None, str)
            self.ui.lineT3BinningOffsetsY.setText(str(value))
            self.ui.lineT3BinningOffsetsY.editingFinished.emit()

            value = setting.value('distRedd', None, str)
            self.ui.lineT3DistRedd.setText(str(value))
            self.ui.lineT3DistRedd.editingFinished.emit()

            value = setting.value('distReddCenterX', None, str)
            self.ui.lineT3DistReddCenterX.setText(str(value))
            self.ui.lineT3DistReddCenterX.editingFinished.emit()

            value = setting.value('distReddCenterY', None, str)
            self.ui.lineT3DistReddCenterY.setText(str(value))
            self.ui.lineT3DistReddCenterY.editingFinished.emit()

            value = setting.value('numberOfBins', None, str)
            self.ui.lineT3NumberOfBins.setText(str(value))
            self.ui.lineT3NumberOfBins.editingFinished.emit()

            value = setting.value('zMin', None, str)
            self.ui.lineT3ZMin.setText(str(value))
            self.ui.lineT3ZMin.editingFinished.emit()

            value = setting.value('zMax', None, str)
            self.ui.lineT3ZMax.setText(str(value))
            self.ui.lineT3ZMax.editingFinished.emit()

            value = setting.value('zSun', None, str)
            self.ui.lineT3ZSun.setText(str(value))
            self.ui.lineT3ZSun.editingFinished.emit()

            value = setting.value('numberIterations', None, str)
            self.ui.lineT3NumberIterations.setText(str(value))
            self.ui.lineT3NumberIterations.editingFinished.emit()
            value = setting.value('numberProcessors', None, str)
            self.ui.lineT3NumberProcessors.setText(str(value))
            self.ui.lineT3NumberProcessors.editingFinished.emit()
            # << Others
            setting.endGroup()

        # >> Load settings file
        file_name = QFileDialog.getOpenFileName(
                parent=self,
                caption=QCoreApplication.translate('MainWindow', 'Open setting file'),
                directory='.',
                filter=QCoreApplication.translate('MainWindow', 'INI files (*.ini);;'
                                                                'All files (*.*)')
        )
        if not file_name.isEmpty():
            self.resetAppState()
            settings = QSettings(file_name, QSettings.IniFormat)
            # Windows
            load_windows(settings)
            # Observed diagram
            load_observed(settings)
            # Model diagram
            load_model(settings)
            # Bundles
            load_bundles(settings)
            # Tables
            load_tables(settings)
            # Others
            load_others(settings)

    @pyqtSlot()
    def save_settings(self):
        file_name = QFileDialog.getSaveFileName(
                parent=self,
                caption=QCoreApplication.translate('MainWindow', 'Save setting file'),
                directory='.',
                filter=QCoreApplication.translate('MainWindow', 'INI files (*.ini);;'
                                                                'All files (*.*)')
        )
        if not file_name.isEmpty():
            settings = QSettings(file_name, QSettings.IniFormat)
            # Windows
            settings.beginGroup('Windows')
            settings.setValue('geometry', self.geometry())
            settings.endGroup()
            # >> Observed diagram
            settings.beginGroup('Observed')
            settings.setValue('obsFile', self.observedDiagram.stdPythonPathToFile)
            settings.setValue('obsExpXaxis', self.strT1ObsExpXaxisActual)
            settings.setValue('obsExpYaxis', self.strT1ObsExpYaxisActual)
            settings.setValue('obsLabXaxis', self.ui.lineT1ObsLabXaxis.text())
            settings.setValue('obsLabYaxis', self.ui.lineT1ObsLabYaxis.text())
            settings.endGroup()
            # >> Model diagram
            settings.beginGroup('Model')
            settings.setValue('modFile', self.modelDiagram.stdPythonPathToFile)
            settings.setValue('modExpXaxis', self.strT1ModExpXaxisActual)
            settings.setValue('modExpYaxis', self.strT1ModExpYaxisActual)
            settings.setValue('modExpAge', self.strT1ModExpAgeActual)
            settings.setValue('modExpMetal', self.strT1ModExpMetallicityActual)
            settings.setValue('modLabXaxis', self.ui.lineT1ModLabXaxis.text())
            settings.setValue('modLabYaxis', self.ui.lineT1ModLabYaxis.text())
            settings.endGroup()
            # >> Bundles
            settings.beginGroup('Bundles')
            settings.setValue('count', Bundle.instances_count)
            settings.beginWriteArray('bundle')
            # >> bundle
            for index_bundle, bundle in enumerate(Bundle.instances):
                settings.setArrayIndex(index_bundle)
                settings.setValue('closed', bundle.closed)
                settings.setValue('len', bundle.len)
                settings.setValue('binSizeX', bundle.bundleBinSizeX)
                settings.setValue('binSizeY', bundle.bundleBinSizeY)
                # >> bundle.points
                settings.beginWriteArray('point')
                for index_point in range(len(bundle.x_points)):
                    settings.setArrayIndex(index_point)
                    settings.setValue('X', bundle.x_points[index_point])
                    settings.setValue('Y', bundle.y_points[index_point])
                # << bundle.points
                settings.endArray();
            # << bundle
            settings.endArray();
            # << Bundles
            settings.endGroup()
            # >> Tables
            settings.beginGroup('Tables')
            settings.setValue('age', self.ui.lineT3AgeRanges.text())
            settings.setValue('metallicity', self.ui.lineT3MetallicityRanges.text())
            # << Tables
            settings.endGroup()
            # >> Others
            settings.beginGroup('Others')
            settings.setValue('refineGrid', self.ui.checkboxT3RefineGrid.isChecked())
            settings.setValue('refineAge', self.ui.checkboxT3RefineAge.isChecked())
            settings.setValue('intMother', self.ui.lineT3IntMother.text())
            settings.setValue('areaReg', self.ui.lineT3AreaReg.text())
            settings.setValue('binningOffsetsX', self.ui.lineT3BinningOffsetsX.text())
            settings.setValue('binningOffsetsY', self.ui.lineT3BinningOffsetsY.text())
            settings.setValue('distRedd', self.ui.lineT3DistRedd.text())
            settings.setValue('distReddCenterX', self.ui.lineT3DistReddCenterX.text())
            settings.setValue('distReddCenterY', self.ui.lineT3DistReddCenterY.text())
            settings.setValue('numberOfBins', self.ui.lineT3NumberOfBins.text())
            settings.setValue('zMin', self.ui.lineT3ZMin.text())
            settings.setValue('zMax', self.ui.lineT3ZMax.text())
            settings.setValue('zSun', self.ui.lineT3ZSun.text())
            settings.setValue('numberIterations', self.ui.lineT3NumberIterations.text())
            settings.setValue('numberProcessors', self.ui.lineT3NumberProcessors.text())
            # << Others
            settings.endGroup()

    @pyqtSlot()
    def loadObservedDiagram(self, parameters_path=None):
        # Clear Observed Lines Edits
        self.clearObservedLinesEdits()
        # Clear Tab2
        self.clearTab2()
        # Clear Observed diagram
        self.observedDiagram.clearContents()
        # Clear current expressions
        self.strT1ObsExpXaxisActual = ''
        self.strT1ObsExpYaxisActual = ''
        # Get file path
        self.observedDiagram.stdPythonPathToFile = parameters_path
        if self.observedDiagram.stdPythonPathToFile is None:
            self.observedDiagram.stdPythonPathToFile = QFileDialog.getOpenFileName(
                    parent=self,
                    caption=QCoreApplication.translate('MainWindow', 'Open observed diagram'),
                    directory='.',
                    filter=QCoreApplication.translate('MainWindow', 'FITS files (*.fits);;'
                                                                    'All files (*.*)')
            )
        self.observedDiagram.setNames(self.observedDiagram.stdPythonPathToFile)
        #
        if self.observedDiagram.readData() == 1:
            self.ui.lineT1ObsFile.insert(self.observedDiagram.filename)
            self.ui.lineT1ObsXaxis.setEnabled(True)
            self.ui.lineT1ObsYaxis.setEnabled(True)
            self.ui.lineT1ObsLabXaxis.setEnabled(True)
            self.ui.lineT1ObsLabYaxis.setEnabled(True)

    def clearObservedLinesEdits(self):
        # Clear QLineEdit
        self.ui.lineT1ObsFile.clear()
        self.ui.lineT1ObsXaxis.clear()
        self.ui.lineT1ObsYaxis.clear()
        self.ui.lineT1ObsLabXaxis.clear()
        self.ui.lineT1ObsLabYaxis.clear()
        # Set empty style QLineEdit
        self.ui.lineT1ObsXaxis.setStyleSheet(self.strAllEmptyLineEdit)
        self.ui.lineT1ObsYaxis.setStyleSheet(self.strAllEmptyLineEdit)
        self.ui.lineT1ObsLabXaxis.setStyleSheet(self.strAllEmptyLineEdit)
        self.ui.lineT1ObsLabYaxis.setStyleSheet(self.strAllEmptyLineEdit)
        # Disable QLineEdit
        self.ui.lineT1ObsXaxis.setDisabled(True)
        self.ui.lineT1ObsYaxis.setDisabled(True)
        self.ui.lineT1ObsLabXaxis.setDisabled(True)
        self.ui.lineT1ObsLabYaxis.setDisabled(True)

    @pyqtSlot()
    def loadModelDiagram(self, parameters_path=None):
        # Clear Observed Lines Edits
        self.clearModelLinesEdits()
        # Clear Tab2, Age and Metallicity elements
        self.clearTab2()
        self.clearAgeMetallicityElements()
        # Clear Model diagram
        self.modelDiagram.clearContents()
        # Clear current expressions
        self.strT1ModExpXaxisActual = ''
        self.strT1ModExpYaxisActual = ''
        self.strT1ModExpAgeActual = ''
        self.strT1ModExpMetallicityActual = ''
        # Get file path
        self.modelDiagram.stdPythonPathToFile = parameters_path
        if self.modelDiagram.stdPythonPathToFile is None:
            self.modelDiagram.stdPythonPathToFile = QFileDialog.getOpenFileName(
                    parent=self,
                    caption=QCoreApplication.translate('MainWindow', 'Open model diagram'),
                    directory='.',
                    filter=QCoreApplication.translate('MainWindow', 'FITS files (*.fits);;'
                                                                    'All files (*.*)')
            )
        self.modelDiagram.setNames(self.modelDiagram.stdPythonPathToFile)
        #
        if (self.modelDiagram.readData() == 1):
            self.ui.lineT1ModFile.insert(self.modelDiagram.filename)
            self.ui.lineT1ModXaxis.setEnabled(True)
            self.ui.lineT1ModYaxis.setEnabled(True)
            self.ui.lineT1ModAge.setEnabled(True)
            self.ui.lineT1ModMetallicity.setEnabled(True)
            self.ui.lineT1ModLabXaxis.setEnabled(True)
            self.ui.lineT1ModLabYaxis.setEnabled(True)

    def clearModelLinesEdits(self):
        # Clear QLineEdit
        self.ui.lineT1ModFile.clear()
        self.ui.lineT1ModXaxis.clear()
        self.ui.lineT1ModYaxis.clear()
        self.ui.lineT1ModAge.clear()
        self.ui.lineT1ModMetallicity.clear()
        self.ui.lineT1ModLabXaxis.clear()
        self.ui.lineT1ModLabYaxis.clear()
        # Set empty style QLineEdit
        self.ui.lineT1ModXaxis.setStyleSheet(self.strAllEmptyLineEdit)
        self.ui.lineT1ModYaxis.setStyleSheet(self.strAllEmptyLineEdit)
        self.ui.lineT1ModAge.setStyleSheet(self.strAllEmptyLineEdit)
        self.ui.lineT1ModMetallicity.setStyleSheet(self.strAllEmptyLineEdit)
        self.ui.lineT1ModLabXaxis.setStyleSheet(self.strAllEmptyLineEdit)
        self.ui.lineT1ModLabYaxis.setStyleSheet(self.strAllEmptyLineEdit)
        # Disable QLineEdit
        self.ui.lineT1ModXaxis.setDisabled(True)
        self.ui.lineT1ModYaxis.setDisabled(True)
        self.ui.lineT1ModAge.setDisabled(True)
        self.ui.lineT1ModMetallicity.setDisabled(True)
        self.ui.lineT1ModLabXaxis.setDisabled(True)
        self.ui.lineT1ModLabYaxis.setDisabled(True)

    @pyqtSlot()
    def loadObservedDiagramXaxis(self, parameters_exp=None):
        if parameters_exp is None:
            exp = str(self.ui.lineT1ObsXaxis.text())
        else:
            exp = parameters_exp
            self.ui.lineT1ObsXaxis.insert(parameters_exp)
        if exp != self.strT1ObsExpXaxisActual:
            self.clearTab2()
            self.strT1ObsExpXaxisActual = exp
            self.observedDiagram.deleteKey('X')
            if len(self.strT1ObsExpXaxisActual) == 0:
                self.ui.lineT1ObsXaxis.setStyleSheet(self.strAllEmptyLineEdit)
                return
            error = self.observedDiagram.calculateExpression(self.strT1ObsExpXaxisActual, 'X')
            if error == 0:
                self.ui.lineT1ObsXaxis.setStyleSheet(self.strAllCorrectLineEdit)
            elif error == 1:
                self.dialog.customizeLabelErrorMessage('La expresion introducida no es correcta...')
                self.dialog.exec_()
                self.ui.lineT1ObsXaxis.setStyleSheet(self.strAllIncorrectLineEdit)
            else: # error == 2
                self.dialog.customizeLabelErrorMessage('Alguna columna de la expresion no existe en el fichero...')
                self.dialog.exec_()
                self.ui.lineT1ObsXaxis.setStyleSheet(self.strAllIncorrectLineEdit)
            if self.modelDiagram.expressionDictionary:
                if (('X' in self.observedDiagram.expressionDictionary) and
                        ('Y' in self.observedDiagram.expressionDictionary) and
                        ('X' in self.modelDiagram.expressionDictionary) and
                        ('Y' in self.modelDiagram.expressionDictionary)):
                    self.activateTab2()
                    self.callingPlotCMDs()
                    self.callingPlotCMDsLabel()

    @pyqtSlot()
    def loadObservedDiagramYaxis(self, parameters_exp=None):
        if parameters_exp is None:
            exp = str(self.ui.lineT1ObsYaxis.text())
        else:
            exp = parameters_exp
            self.ui.lineT1ObsYaxis.insert(parameters_exp)
        if exp != self.strT1ObsExpYaxisActual:
            self.clearTab2()
            self.strT1ObsExpYaxisActual = exp
            self.observedDiagram.deleteKey('Y')
            if len(self.strT1ObsExpYaxisActual) == 0:
                self.ui.lineT1ObsYaxis.setStyleSheet(self.strAllEmptyLineEdit)
                return
            error = self.observedDiagram.calculateExpression(self.strT1ObsExpYaxisActual, 'Y')
            if error == 0:
                self.ui.lineT1ObsYaxis.setStyleSheet(self.strAllCorrectLineEdit)
            elif error == 1:
                self.dialog.customizeLabelErrorMessage('La expresion introducida no es correcta...')
                self.dialog.exec_()
                self.ui.lineT1ObsYaxis.setStyleSheet(self.strAllIncorrectLineEdit)
            else: # error == 2
                self.dialog.customizeLabelErrorMessage('Alguna columna de la expresion no existe en el fichero...')
                self.dialog.exec_()
                self.ui.lineT1ObsYaxis.setStyleSheet(self.strAllIncorrectLineEdit)
            if self.modelDiagram.expressionDictionary:
                if (('X' in self.observedDiagram.expressionDictionary) and
                        ('Y' in self.observedDiagram.expressionDictionary) and
                        ('X' in self.modelDiagram.expressionDictionary) and
                        ('Y' in self.modelDiagram.expressionDictionary)):
                    self.activateTab2()
                    self.callingPlotCMDs()
                    self.callingPlotCMDsLabel()

    @pyqtSlot()
    def loadModelDiagramXaxis(self, parameters_exp=None):
        if parameters_exp is None:
            exp = str(self.ui.lineT1ModXaxis.text())
        else:
            exp = parameters_exp
            self.ui.lineT1ModXaxis.insert(parameters_exp)
        if exp != self.strT1ModExpXaxisActual:
            self.clearTab2()
            self.strT1ModExpXaxisActual = exp
            self.modelDiagram.deleteKey('X')
            if len(self.strT1ModExpXaxisActual) == 0:
                self.ui.lineT1ModXaxis.setStyleSheet(self.strAllEmptyLineEdit)
                return
            error = self.modelDiagram.calculateExpression(self.strT1ModExpXaxisActual, 'X')
            if error == 0:
                self.ui.lineT1ModXaxis.setStyleSheet(self.strAllCorrectLineEdit)
            elif error == 1:
                self.dialog.customizeLabelErrorMessage('La expresion introducida no es correcta...')
                self.dialog.exec_()
                self.ui.lineT1ModXaxis.setStyleSheet(self.strAllIncorrectLineEdit)
            else: # error == 2
                self.dialog.customizeLabelErrorMessage('Alguna columna de la expresion no existe en el fichero...')
                self.dialog.exec_()
                self.ui.lineT1ModXaxis.setStyleSheet(self.strAllIncorrectLineEdit)
            if self.observedDiagram.expressionDictionary:
                if (('X' in self.observedDiagram.expressionDictionary) and
                        ('Y' in self.observedDiagram.expressionDictionary) and
                        ('X' in self.modelDiagram.expressionDictionary) and
                        ('Y' in self.modelDiagram.expressionDictionary)):
                    self.activateTab2()
                    self.callingPlotCMDs()
                    self.callingPlotCMDsLabel()

    @pyqtSlot()
    def loadModelDiagramYaxis(self, parameters_exp=None):
        if parameters_exp is None:
            exp = str(self.ui.lineT1ModYaxis.text())
        else:
            exp = parameters_exp
            self.ui.lineT1ModYaxis.insert(parameters_exp)
        if exp != self.strT1ModExpYaxisActual:
            self.clearTab2()
            self.strT1ModExpYaxisActual = exp
            self.modelDiagram.deleteKey('Y')
            if len(self.strT1ModExpYaxisActual) == 0:
                self.ui.lineT1ModYaxis.setStyleSheet(self.strAllEmptyLineEdit)
                return
            error = self.modelDiagram.calculateExpression(self.strT1ModExpYaxisActual, 'Y')
            if error == 0:
                self.ui.lineT1ModYaxis.setStyleSheet(self.strAllCorrectLineEdit)
            elif error == 1:
                self.dialog.customizeLabelErrorMessage('La expresion introducida no es correcta...')
                self.dialog.exec_()
                self.ui.lineT1ModYaxis.setStyleSheet(self.strAllIncorrectLineEdit)
            else: # error == 2
                self.dialog.customizeLabelErrorMessage('Alguna columna de la expresion no existe en el fichero...')
                self.dialog.exec_()
                self.ui.lineT1ModYaxis.setStyleSheet(self.strAllIncorrectLineEdit)
            if self.observedDiagram.expressionDictionary:
                if (('X' in self.observedDiagram.expressionDictionary) and
                        ('Y' in self.observedDiagram.expressionDictionary) and
                        ('X' in self.modelDiagram.expressionDictionary) and
                        ('Y' in self.modelDiagram.expressionDictionary)):
                    self.activateTab2()
                    self.callingPlotCMDs()
                    self.callingPlotCMDsLabel()

    @pyqtSlot()
    def loadModelDiagramAge(self, parameters_exp=None):
        if parameters_exp is None:
            exp = str(self.ui.lineT1ModAge.text())
        else:
            exp = parameters_exp
            self.ui.lineT1ModAge.insert(parameters_exp)
        if exp != self.strT1ModExpAgeActual:
            self.strT1ModExpAgeActual = exp
            self.modelDiagram.deleteKey('Age')
            self.ui.tableT3Age.model().removeRows(0, self.ui.tableT3Age.model().rowCount())
            if len(self.strT1ModExpAgeActual) == 0:
                self.ui.tableT3Age.setDisabled(True)
                self.ui.lineT3AgeRanges.setDisabled(True)
                self.ui.lineT3AgeRanges.clear()
                self.ui.lineT1ModAge.setStyleSheet(self.strAllEmptyLineEdit)
                return
            error = self.modelDiagram.calculateExpression(self.strT1ModExpAgeActual, 'Age')
            if error == 0:
                self.ui.lineT1ModAge.setStyleSheet(self.strAllCorrectLineEdit)
            elif error == 1:
                self.dialog.customizeLabelErrorMessage('La expresion introducida no es correcta...')
                self.dialog.exec_()
                self.ui.lineT1ModAge.setStyleSheet(self.strAllIncorrectLineEdit)
            else: # error == 2
                self.dialog.customizeLabelErrorMessage('Alguna columna de la expresion no existe en el fichero...')
                self.dialog.exec_()
                self.ui.lineT1ModAge.setStyleSheet(self.strAllIncorrectLineEdit)
            if 'Age' in self.modelDiagram.expressionDictionary:
                self.ui.lineT3AgeRanges.setEnabled(True)
            else:
                self.ui.tableT3Age.setDisabled(True)
                self.ui.lineT3AgeRanges.setDisabled(True)
                self.ui.lineT3AgeRanges.clear()

    @pyqtSlot()
    def loadModelDiagramMetallicity(self, parameters_exp=None):
        if parameters_exp is None:
            exp = str(self.ui.lineT1ModMetallicity.text())
        else:
            exp = parameters_exp
            self.ui.lineT1ModMetallicity.insert(parameters_exp)
        if exp != self.strT1ModExpMetallicityActual:
            self.strT1ModExpMetallicityActual = exp
            self.modelDiagram.deleteKey('Metallicity')
            self.ui.tableT3Metallicity.model().removeRows(0, self.ui.tableT3Metallicity.model().rowCount())
            if len(self.strT1ModExpMetallicityActual) == 0:
                self.ui.tableT3Metallicity.setDisabled(True)
                self.ui.lineT3MetallicityRanges.setDisabled(True)
                self.ui.lineT3MetallicityRanges.clear()
                self.ui.lineT1ModMetallicity.setStyleSheet(self.strAllEmptyLineEdit)
                return
            error = self.modelDiagram.calculateExpression(self.strT1ModExpMetallicityActual, 'Metallicity')
            if error == 0:
                self.ui.lineT1ModMetallicity.setStyleSheet(self.strAllCorrectLineEdit)
            elif error == 1:
                self.dialog.customizeLabelErrorMessage('La expresion introducida no es correcta...')
                self.dialog.exec_()
                self.ui.lineT1ModMetallicity.setStyleSheet(self.strAllIncorrectLineEdit)
            else: # error == 2
                self.dialog.customizeLabelErrorMessage('Alguna columna de la expresion no existe en el fichero...')
                self.dialog.exec_()
                self.ui.lineT1ModMetallicity.setStyleSheet(self.strAllIncorrectLineEdit)
            if 'Metallicity' in self.modelDiagram.expressionDictionary:
                self.ui.lineT3MetallicityRanges.setEnabled(True)
            else:
                self.ui.tableT3Metallicity.setDisabled(True)
                self.ui.lineT3MetallicityRanges.setDisabled(True)
                self.ui.lineT3MetallicityRanges.clear()

    def clearTab2(self):
        # desactivando la pestaña 2
        self.ui.tabWidget.setTabEnabled(1, False)
        # desactivando los elementos que intervienen en la creacion de bundles
        self.ui.buttonT2BundleOpen.setDisabled(True)
        self.ui.buttonT2BundleClose.setDisabled(True)
        self.ui.buttonT2BundleRemove.setDisabled(True)
        self.ui.buttonT2PointCreate.setDisabled(True)
        self.ui.buttonT2PointModify.setDisabled(True)
        self.ui.buttonT2PointRemove.setDisabled(True)
        self.ui.spinT2PointX.setDisabled(True)
        self.ui.spinT2PointY.setDisabled(True)
        self.ui.spinT2PointX.clear()
        self.ui.spinT2PointY.clear()
        self.ui.comboT2Bundle.setDisabled(True)
        self.ui.comboT2Bundle.currentIndexChanged.disconnect(self.bundle_selected)
        self.ui.comboT2Bundle.clear()
        self.ui.comboT2Bundle.currentIndexChanged.connect(self.bundle_selected)
        self.ui.bundleGroupBox.setTitle('Select a Bundle')
        self.ui.tableT2Bundle.setDisabled(True)
        self.ui.tableT2Bundle.model().removeRows(0, self.ui.tableT2Bundle.model().rowCount())
        self.ui.spinT2BundleBinSizeX.setDisabled(True)
        self.ui.spinT2BundleBinSizeY.setDisabled(True)
        self.ui.spinT2BundleBinSizeX.clear()
        self.ui.spinT2BundleBinSizeY.clear()
        #
        Bundle.remove_bundles()
        self.dictT2BundleSelected = {'instance': None, 'found': False, 'index': -1, 'row': -1}
        self.diagramT2Obs.clear_plot()
        self.diagramT2Mod.clear_plot()
        #
        self.ui.lineT2MinX.clear()
        self.ui.lineT2MaxX.clear()
        self.ui.lineT2MinY.clear()
        self.ui.lineT2MaxY.clear()
        self.ui.spinT2BinSizeX.clear()
        self.ui.spinT2BinSizeY.clear()
        #
        self.ui.checkboxT2colorbar.stateChanged.disconnect(self.callingPlotCMDs)
        self.ui.checkboxT2colorbar.setChecked(False)
        self.ui.checkboxT2colorbar.stateChanged.connect(self.callingPlotCMDs)
        #
        self.ui.comboT2Scale.currentIndexChanged.disconnect(self.callingPlotCMDs)
        self.ui.comboT2Scale.setCurrentIndex(0)
        self.ui.comboT2Scale.currentIndexChanged.connect(self.callingPlotCMDs)
        # Deshabilitar las QAccions y QDockWidgets de la tab2
        # Desactivar Diagrama Tools
        self.ui.dockT2DiagramTools.setVisible(False)
        self.ui.actionDiagram.setChecked(False)
        self.ui.actionDiagram.setDisabled(True)
        # Desactivar BundleTools
        self.ui.dockT2BundleTools.setVisible(False)
        self.ui.actionBundle.setChecked(False)
        self.ui.actionBundle.setDisabled(True)
        #
        self.ui.dockT2HistogramTools.setVisible(False)
        self.ui.actionHistogram.setChecked(False)
        self.ui.actionHistogram.setDisabled(True)

    def activateTab2(self):
        # activando pestaña 2
        self.ui.tabWidget.setTabEnabled(1, True)
        # estado incial de los distintos elementos de la pestaña 2
        self.ui.buttonT2BundleCreate.setEnabled(True)

    @pyqtSlot(int)
    def activateTools(self, index):
        if index == 1:  # tab2 clicked
            # Habilitar las QAccions y QDockWidgets de la tab2
            # Activar Diagrama Tools
            self.ui.actionDiagram.setEnabled(True)
            # Activar BundleTools
            self.ui.actionBundle.setEnabled(True)
            # Activar HistogramTools
            if ((self.observedDiagram.expressionDictionary) and
                    ('X' in self.observedDiagram.expressionDictionary) and
                    ('Y' in self.observedDiagram.expressionDictionary) and
                    ('X' in self.modelDiagram.expressionDictionary) and
                    ('Y' in self.modelDiagram.expressionDictionary) and
                    (self.isThereAnyBundleComplete() == True)):
                self.ui.actionHistogram.setEnabled(True)
                self.ui.buttonDockHistDraw.setEnabled(True)
            else:
                self.ui.actionHistogram.setDisabled(True)
                self.ui.buttonDockHistDraw.setDisabled(True)
                # borrar histograma del diagrama observado
                self.histogramT2Obs.clear()
                # borrar histograma del diagrama modelo
                self.histogramT2Mod.clear()
        else:
            # Deshabilitar las QAccions y QDockWidgets de la tab2
            # Desactivar Diagrama Tools
            self.ui.dockT2DiagramTools.setVisible(False)
            self.ui.actionDiagram.setChecked(False)
            self.ui.actionDiagram.setDisabled(True)
            # Desactivar BundleTools
            self.ui.dockT2BundleTools.setVisible(False)
            self.ui.actionBundle.setChecked(False)
            self.ui.actionBundle.setDisabled(True)
            # Desactivar HistogramTools
            self.ui.dockT2HistogramTools.setVisible(False)
            self.ui.actionHistogram.setChecked(False)
            self.ui.actionHistogram.setDisabled(True)

    @pyqtSlot(float)
    def saveBundleBinSizeX(self, value):
        self.dictT2BundleSelected['instance'].bundleBinSizeX = value
        if self.isThereAnyBundleComplete() == False:
            self.ui.actionHistogram.setDisabled(True)
            self.ui.buttonDockHistDraw.setDisabled(True)
            self.histogramT2Obs.clear()
            self.histogramT2Mod.clear()
        elif self.ui.tabWidget.currentIndex() == 1:  # tab 2
            self.ui.actionHistogram.setEnabled(True)
            self.ui.buttonDockHistDraw.setEnabled(True)
            self.histogramT2Obs.clear()
            self.histogramT2Mod.clear()

    @pyqtSlot(float)
    def saveBundleBinSizeY(self, value):
        self.dictT2BundleSelected['instance'].bundleBinSizeY = value
        if self.isThereAnyBundleComplete() == False:
            self.ui.actionHistogram.setDisabled(True)
            self.ui.buttonDockHistDraw.setDisabled(True)
            self.histogramT2Obs.clear()
            self.histogramT2Mod.clear()
        elif self.ui.tabWidget.currentIndex() == 1:
            self.ui.actionHistogram.setEnabled(True)
            self.ui.buttonDockHistDraw.setEnabled(True)
            self.histogramT2Obs.clear()
            self.histogramT2Mod.clear()

    @pyqtSlot(dict)
    def diagram_events(self, event):
        bundle = self.dictT2BundleSelected['instance']
        found = self.dictT2BundleSelected['found']
        index = self.dictT2BundleSelected['index']
        if found is False or bundle.closed is True:
            return
        button = event['button']
        xdata = round(event['xdata'], C.NUM_DECIMAL_DIGITS_POINTS)
        ydata = round(event['ydata'], C.NUM_DECIMAL_DIGITS_POINTS)
        if button is C.LEFT_CLICK:
            # Confirmar que el punto no esté ya definido en el bundle actual
            x_points, y_points = bundle.x_points, bundle.y_points
            for i in range(0, len(x_points), 1):
                if xdata == x_points[i] and ydata == y_points[i]:
                    return
            bundle.point_append(xdata, ydata)
            Diagram.line_edit(index, bundle.x_points, bundle.y_points)
            self.ui.spinT2PointX.clear()
            self.ui.spinT2PointY.clear()
            self.ui.buttonT2PointCreate.setDisabled(True)
            self.ui.spinT2PointX.setDisabled(True)
            self.ui.spinT2PointY.setDisabled(True)
            self.table_refresh(bundle)
            return
        elif button is C.RIGHT_CLICK:
            self.ui.spinT2PointX.setEnabled(True)
            self.ui.spinT2PointY.setEnabled(True)
            self.ui.spinT2PointX.setValue(xdata)
            self.ui.spinT2PointY.setValue(ydata)
            self.ui.buttonT2PointCreate.setEnabled(True)
            return
        else:
            return

    @pyqtSlot()
    def bundle_create(self):
        # Crear nuevo contenedor.
        bundle = Bundle.create()
        bundle.status.connect(self.bundle_check_status)
        Diagram.line_create()
        # Añadir bundle al ComboBox.
        self.ui.comboT2Bundle.addItem('Bundle %02d' % Bundle.instances_count)
        # Definir los estados de salida
        self.ui.buttonT2BundleCreate.setEnabled(True)
        self.ui.buttonT2BundleOpen.setDisabled(True)
        self.ui.buttonT2BundleClose.setDisabled(True)
        self.ui.buttonT2BundleRemove.setEnabled(True)
        self.ui.comboT2Bundle.setEnabled(True)
        self.ui.tableT2Bundle.setEnabled(True)
        self.ui.spinT2BundleBinSizeX.setEnabled(True)
        self.ui.spinT2BundleBinSizeY.setEnabled(True)
        self.ui.spinT2PointX.clear()
        self.ui.spinT2PointY.clear()
        self.ui.spinT2PointX.setDisabled(True)
        self.ui.spinT2PointY.setDisabled(True)
        self.ui.buttonT2PointCreate.setDisabled(True)
        self.ui.buttonT2PointModify.setDisabled(True)
        self.ui.buttonT2PointRemove.setDisabled(True)
        # Colorear fondo del item creado en el ComboBox para indicar el estado
        index = self.ui.comboT2Bundle.count() - 1
        self.ui.comboT2Bundle.setItemData(index,
                                          QColor(Qt.red),
                                          Qt.BackgroundRole)
        # Hacer focus en el ComboBox sobre el nuevo contedor creado.
        self.ui.comboT2Bundle.setCurrentIndex(index)

    @pyqtSlot(object, str)
    def bundle_check_status(self, bundle):
        index = self.dictT2BundleSelected['index']
        # Comprobar estado del bundle
        if not bundle.closed:
            # El bundle está abierto
            Diagram.canvas_connect()
            self.ui.buttonT2BundleOpen.setDisabled(True)
            self.ui.buttonT2BundleClose.setEnabled(True)
            if bundle.len >= Bundle.min_points:
                # El bundle está listo para cerrarse
                self.ui.buttonT2BundleClose.setEnabled(True)
                self.ui.comboT2Bundle.setItemData(index,
                                                  QColor(Qt.yellow),
                                                  Qt.BackgroundRole)
            else:
                # Aún no es posible cerrar el bundle
                self.ui.buttonT2BundleClose.setDisabled(True)
                self.ui.comboT2Bundle.setItemData(index,
                                                  QColor(Qt.red),
                                                  Qt.BackgroundRole)
        else:
            # El bundle está cerrado
            Diagram.canvas_disconnect()
            self.ui.buttonT2BundleOpen.setEnabled(True)
            self.ui.buttonT2BundleClose.setDisabled(True)
            self.ui.comboT2Bundle.setItemData(index,
                                              QColor(Qt.green),
                                              Qt.BackgroundRole)

    @pyqtSlot()
    def bundle_open(self):
        # Cerrar el bundle actual
        index = self.dictT2BundleSelected['index']
        bundle = self.dictT2BundleSelected['instance']
        bundle.open()
        Diagram.line_edit(index, bundle.x_points, bundle.y_points)
        Diagram.point_highlight(bundle.x_points, bundle.y_points, True)
        # Controlar el estado de la generacion de histogramas
        if self.isThereAnyBundleComplete() == False:
            self.ui.actionHistogram.setDisabled(True)
            self.ui.buttonDockHistDraw.setDisabled(True)
            self.histogramT2Obs.clear()
            self.histogramT2Mod.clear()
        # Definir los estados de salida
        self.ui.buttonT2BundleCreate.setEnabled(True)
        self.ui.buttonT2BundleOpen.setDisabled(True)
        self.ui.buttonT2BundleClose.setEnabled(True)
        self.ui.buttonT2BundleRemove.setEnabled(True)
        self.ui.comboT2Bundle.setEnabled(True)
        self.ui.tableT2Bundle.setEnabled(True)
        self.ui.spinT2BundleBinSizeX.setEnabled(True)
        self.ui.spinT2BundleBinSizeY.setEnabled(True)
        self.ui.spinT2PointX.clear()
        self.ui.spinT2PointY.clear()
        self.ui.spinT2PointX.setDisabled(True)
        self.ui.spinT2PointY.setDisabled(True)
        self.ui.buttonT2PointCreate.setDisabled(True)
        self.ui.buttonT2PointModify.setDisabled(True)
        self.ui.buttonT2PointRemove.setDisabled(True)

    @pyqtSlot()
    def bundle_close(self):
        # Cerrar el bundle actual
        index = self.dictT2BundleSelected['index']
        bundle = self.dictT2BundleSelected['instance']
        bundle.close()
        Diagram.line_edit(index, bundle.x_points, bundle.y_points)
        # Controlar el estado de la generacion de histogramas
        if self.isThereAnyBundleComplete() == True and \
                        self.ui.tabWidget.currentIndex() == 1:
            self.ui.actionHistogram.setEnabled(True)
            self.ui.buttonDockHistDraw.setEnabled(True)
            self.histogramT2Obs.clear()
            self.histogramT2Mod.clear()
        # Definir los estados de salida
        self.ui.buttonT2BundleCreate.setEnabled(True)
        self.ui.buttonT2BundleOpen.setEnabled(True)
        self.ui.buttonT2BundleClose.setDisabled(True)
        self.ui.buttonT2BundleRemove.setEnabled(True)
        self.ui.comboT2Bundle.setEnabled(True)
        self.ui.tableT2Bundle.setEnabled(True)
        self.ui.spinT2BundleBinSizeX.setEnabled(True)
        self.ui.spinT2BundleBinSizeY.setEnabled(True)
        self.ui.spinT2PointX.clear()
        self.ui.spinT2PointY.clear()
        self.ui.spinT2PointX.setDisabled(True)
        self.ui.spinT2PointY.setDisabled(True)
        self.ui.buttonT2PointCreate.setDisabled(True)
        self.ui.buttonT2PointModify.setDisabled(True)
        self.ui.buttonT2PointRemove.setDisabled(True)

    @pyqtSlot()
    def bundle_remove(self):
        # Definir los estados de salida
        self.ui.buttonT2BundleCreate.setEnabled(True)
        self.ui.buttonT2BundleClose.setDisabled(True)
        self.ui.buttonT2BundleRemove.setEnabled(True)
        self.ui.comboT2Bundle.setEnabled(True)
        self.ui.tableT2Bundle.setEnabled(True)
        self.ui.spinT2BundleBinSizeX.setEnabled(True)
        self.ui.spinT2BundleBinSizeY.setEnabled(True)
        self.ui.spinT2PointX.clear()
        self.ui.spinT2PointY.clear()
        self.ui.spinT2PointX.setDisabled(True)
        self.ui.spinT2PointY.setDisabled(True)
        self.ui.buttonT2PointCreate.setDisabled(True)
        self.ui.buttonT2PointModify.setDisabled(True)
        self.ui.buttonT2PointRemove.setDisabled(True)
        # Identificar bundle del ComboBox a eliminar.
        index = self.dictT2BundleSelected['index']
        Bundle.remove(index)
        Diagram.line_remove(index)
        Diagram.update()
        self.ui.comboT2Bundle.removeItem(index)
        # Controlar el estado de la generacion de histogramas
        if self.isThereAnyBundleComplete() == False:
            self.ui.actionHistogram.setDisabled(True)
            self.ui.buttonDockHistDraw.setDisabled(True)
            self.histogramT2Obs.clear()
            self.histogramT2Mod.clear()
        # Continuar definiendo los estados de salida
        #   - ¿Sólo está el elemento por defecto en el ComboBox?
        if self.ui.comboT2Bundle.count() is 0:
            # Quitar resaltado de puntos
            Diagram.point_highlight([], [], visible=False)
            # Reiniciar diccionario
            self.dictT2BundleSelected['instance'] = None
            self.dictT2BundleSelected['found'] = False
            self.dictT2BundleSelected['index'] = -1
            self.dictT2BundleSelected['row'] = -1
            # Desconectar diagramas
            Diagram.canvas_disconnect()
            # Borrar tabla
            model = self.ui.tableT2Bundle.model()
            model.removeRows(0, model.rowCount())
            # Restablecer estados finales del resto de elementos
            self.ui.buttonT2BundleCreate.setEnabled(True)
            self.ui.buttonT2BundleOpen.setDisabled(True)
            self.ui.buttonT2BundleClose.setDisabled(True)
            self.ui.buttonT2BundleRemove.setDisabled(True)
            self.ui.comboT2Bundle.setDisabled(True)
            self.ui.tableT2Bundle.setDisabled(True)
            self.ui.spinT2BundleBinSizeX.setDisabled(True)
            self.ui.spinT2BundleBinSizeY.setDisabled(True)
            self.ui.buttonT2PointCreate.setDisabled(True)
            self.ui.buttonT2PointModify.setDisabled(True)
            self.ui.buttonT2PointRemove.setDisabled(True)

    @pyqtSlot(int)
    def bundle_selected(self, index):
        # Definir index antes que bundle, para evitar error con la señal status
        self.dictT2BundleSelected['index'] = index
        # Buscar bundle
        bundle, found = Bundle.find(index)
        self.dictT2BundleSelected['instance'] = bundle
        self.dictT2BundleSelected['found'] = found
        # Imprimir datos del bundle en la tabla
        self.table_refresh(bundle)
        if found:
            # ¿Bundle cerrado?
            if bundle.closed:
                self.ui.buttonT2BundleClose.setDisabled(True)
                Diagram.canvas_disconnect()
            else:
                if bundle.len >= Bundle.min_points:
                    # Alcanzado número mínimo de puntos para cerrar el bunble
                    self.ui.buttonT2BundleClose.setEnabled(True)
                else:
                    self.ui.buttonT2BundleClose.setDisabled(True)

    @pyqtSlot(QModelIndex)
    def point_table_selected(self, modelindex):
        row = modelindex.row()
        self.dictT2BundleSelected['row'] = row
        xitem = self.ui.tableT2Bundle.item(row, 0)
        yitem = self.ui.tableT2Bundle.item(row, 1)
        xdata = float(xitem.text())
        ydata = float(yitem.text())
        Diagram.point_highlight([xdata], [ydata])
        bundle = self.dictT2BundleSelected['instance']
        self.ui.spinT2PointX.setEnabled(True)
        self.ui.spinT2PointY.setEnabled(True)
        self.ui.spinT2PointX.setValue(xdata)
        self.ui.spinT2PointY.setValue(ydata)
        self.ui.buttonT2PointModify.setEnabled(True)
        self.ui.buttonT2PointRemove.setEnabled(True)

    @pyqtSlot()
    def point_add(self):
        # Coger datos de los SpinBox.
        xdata = self.ui.spinT2PointX.value()
        ydata = self.ui.spinT2PointY.value()
        # Identificar bundle
        bundle = self.dictT2BundleSelected['instance']
        index = self.dictT2BundleSelected['index']
        # Añadir punto
        bundle.point_append(xdata, ydata)
        Diagram.line_edit(index, bundle.x_points, bundle.y_points)
        # Modificar status después de este método.
        self.ui.spinT2PointX.clear()
        self.ui.spinT2PointY.clear()
        self.ui.spinT2PointX.setDisabled(True)
        self.ui.spinT2PointY.setDisabled(True)
        self.ui.buttonT2PointCreate.setDisabled(True)
        # Vizualizar nuevo punto en la tabla.
        self.table_refresh(bundle)

    @pyqtSlot()
    def point_modify(self):
        # Identificar índice del bundle, bundle y fila seleccionada
        bundle = self.dictT2BundleSelected['instance']
        index = self.dictT2BundleSelected['index']
        row = self.dictT2BundleSelected['row']
        # Coger datos de los SpinBox.
        xdata = self.ui.spinT2PointX.value()
        ydata = self.ui.spinT2PointY.value()
        # Modificar punto en el bundle y diagramas
        bundle.point_modify(row, xdata, ydata)
        Diagram.line_edit(index, bundle.x_points, bundle.y_points)
        # Definir estados de salida
        self.ui.spinT2PointX.clear()
        self.ui.spinT2PointY.clear()
        self.ui.spinT2PointX.setDisabled(True)
        self.ui.spinT2PointY.setDisabled(True)
        self.ui.buttonT2PointModify.setDisabled(True)
        self.ui.buttonT2PointRemove.setDisabled(True)
        # Refrescar datos de la tabla
        self.table_refresh(bundle)

    @pyqtSlot()
    def point_remove(self):
        # Identificar índice del bundle, bundle y fila seleccionada
        bundle = self.dictT2BundleSelected['instance']
        index = self.dictT2BundleSelected['index']
        row = self.dictT2BundleSelected['row']
        # Eliminar punto en el bundle y diagramas
        bundle.point_remove(row)
        Diagram.line_edit(index, bundle.x_points, bundle.y_points)
        # Definir estados de salida
        self.ui.spinT2PointX.clear()
        self.ui.spinT2PointY.clear()
        self.ui.spinT2PointX.setDisabled(True)
        self.ui.spinT2PointY.setDisabled(True)
        self.ui.buttonT2PointModify.setDisabled(True)
        self.ui.buttonT2PointRemove.setDisabled(True)
        # Refrescar datos de la tabla
        self.table_refresh(bundle)

    @pyqtSlot()
    def table_refresh(self, bundle):
        # Borrar filas
        model = self.ui.tableT2Bundle.model()
        model.removeRows(0, model.rowCount())
        # Borrar Bundle Bin Size
        self.ui.spinT2BundleBinSizeX.clear()
        self.ui.spinT2BundleBinSizeY.clear()
        # Buscar datos del bundle
        if bundle is None:
            self.ui.bundleGroupBox.setTitle('Select a Bundle')
            return
        x_points, y_points = bundle.x_points, bundle.y_points
        Diagram.point_highlight(x_points, y_points)
        # Estableciendo titulo del bundle groupbox
        self.ui.bundleGroupBox.setTitle(self.ui.comboT2Bundle.currentText())
        # Rellenar tabla
        for i in range(bundle.len):
            item_x = QTableWidgetItem(str(x_points[i]))
            item_y = QTableWidgetItem(str(y_points[i]))
            flags = item_x.flags()
            flags &= ~Qt.ItemIsEditable
            flags &= ~Qt.ItemIsDropEnabled
            item_x.setFlags(flags)
            item_y.setFlags(flags)
            self.ui.tableT2Bundle.insertRow(i)
            self.ui.tableT2Bundle.setItem(i, 0, item_x)
            self.ui.tableT2Bundle.setItem(i, 1, item_y)
        # Rellenar Bundle Bin Size
        valueBinSizeX = bundle.bundleBinSizeX
        valueBinSizeY = bundle.bundleBinSizeY
        self.ui.spinT2BundleBinSizeX.setValue(valueBinSizeX)
        self.ui.spinT2BundleBinSizeY.setValue(valueBinSizeY)

    @pyqtSlot()
    def callingPlotCMDs(self, parameters_xRange=None, parameters_yRange=None):
        if parameters_xRange == None:
            minX = self.ui.lineT2MinX.text().toFloat()
            minXValue = False
            if minX[1] == True:
                minXValue = float(self.ui.lineT2MinX.text())
            maxX = self.ui.lineT2MaxX.text().toFloat()
            maxXValue = False
            if maxX[1] == True:
                maxXValue = float(self.ui.lineT2MaxX.text())
            self.listT2rangeX = []
            if minX[1] == False and maxX[1] == True:
                self.listT2rangeX.append(-0.49)
                self.listT2rangeX.append(maxXValue)
            elif minX[1] == True and maxX[1] == False:
                self.listT2rangeX.append(minXValue)
                self.listT2rangeX.append(2.49)
            elif minX[1] == True and maxX[1] == True:
                self.listT2rangeX.append(minXValue)
                self.listT2rangeX.append(maxXValue)
            else:
                self.listT2rangeX.append(-0.49)
                self.listT2rangeX.append(2.49)
                # FIXME Valores minimo y maximo del histograma (no constantes numericas)
                #   crange=(-0.49,2.49)
        else:
            self.listT2rangeX = parameters_xRange
            # setting default CMD color in line edits
            if self.listT2rangeX[0] != -0.49:
                self.ui.lineT2MinX.setText(str(self.listT2rangeX[0]))
            if self.listT2rangeX[1] != 2.49:
                self.ui.lineT2MaxX.setText(str(self.listT2rangeX[1]))
        self.ui.spinT2PointX.setRange(self.listT2rangeX[0], self.listT2rangeX[1])
        if parameters_yRange == None:
            minY = self.ui.lineT2MinY.text().toFloat()
            minYValue = False
            if minY[1] == True:
                minYValue = float(self.ui.lineT2MinY.text())
            maxY = self.ui.lineT2MaxY.text().toFloat()
            maxYValue = False
            if maxY[1] == True:
                maxYValue = float(self.ui.lineT2MaxY.text())
            self.listT2rangeY = []
            if minY[1] == False and maxY[1] == True:
                self.listT2rangeY.append(maxYValue)
                self.listT2rangeY.append(-4.99)
            elif minY[1] == True and maxY[1] == False:
                self.listT2rangeY.append(4.99)
                self.listT2rangeY.append(minYValue)
            elif minY[1] == True and maxY[1] == True:
                self.listT2rangeY.append(maxYValue)
                self.listT2rangeY.append(minYValue)
            else:
                self.listT2rangeY.append(4.99)  # maximo
                self.listT2rangeY.append(-4.99)  # minimo
                # FIXME Valores minimo y maximo del histograma (no constantes numericas)
                #   mrange=(4.99,-4.99)
        else:
            self.listT2rangeY = parameters_yRange
            # setting default CMD magnitude in line edits
            if self.listT2rangeY[0] != 4.99:
                self.ui.lineT2MaxY.setText(str(self.listT2rangeY[0]))
            if self.listT2rangeY[1] != -4.99:
                self.ui.lineT2MinY.setText(str(self.listT2rangeY[1]))
        self.ui.spinT2PointY.setRange(self.listT2rangeY[1], self.listT2rangeY[0])
        activateToolbars = False
        if self.ui.checkboxT2colorbar.isChecked() == True:
            activateToolbars = True
        scale = self.ui.comboT2Scale.currentIndex()
        binSizeX = self.ui.spinT2BinSizeX.value()
        binSizeY = self.ui.spinT2BinSizeY.value()
        binsizeList = []
        if binSizeX == 0.0 and binSizeY > 0.0:
            binsizeList.append(0.04)
            binsizeList.append(binSizeY)
        elif binSizeX > 0.0 and binSizeY == 0.0:
            binsizeList.append(binSizeX)
            binsizeList.append(0.03)
        elif binSizeX > 0.0 and binSizeY > 0.0:
            binsizeList.append(binSizeX)
            binsizeList.append(binSizeY)
        else:
            binsizeList.append(0.04)
            binsizeList.append(0.03)
        Xrange = tuple(self.listT2rangeX)
        Yrange = tuple(self.listT2rangeY)
        binsize = tuple(binsizeList)
        plot_cmds(self.observedDiagram.expressionDictionary['X'],
                  self.observedDiagram.expressionDictionary['Y'],
                  self.modelDiagram.expressionDictionary['X'],
                  self.modelDiagram.expressionDictionary['Y'],
                  activateToolbars,
                  self.diagramT2Obs,
                  self.diagramT2Mod,
                  Xrange,
                  Yrange,
                  binsize,
                  scale)

    @pyqtSlot()
    def callingPlotCMDsLabel(self):
        # Observed diagram X-Label
        xlabelObs = str(self.ui.lineT1ObsLabXaxis.text())
        if len(xlabelObs) == 0:
            xlabelObs = 'color'
        # Observed diagram Y-Label
        ylabelObs = str(self.ui.lineT1ObsLabYaxis.text())
        if len(ylabelObs) == 0:
            ylabelObs = 'magnitude'
        # Model diagram X-Label
        xlabelMod = str(self.ui.lineT1ModLabXaxis.text())
        if len(xlabelMod) == 0:
            xlabelMod = 'color'
        # Model diagram Y-Label
        ylabelMod = str(self.ui.lineT1ModLabYaxis.text())
        if len(ylabelMod) == 0:
            ylabelMod = 'magnitude'
        # Plot Label Diagrams
        plot_cmds_label(
                self.diagramT2Obs,
                self.diagramT2Mod,
                xlabelObs,
                ylabelObs,
                xlabelMod,
                ylabelMod
        )

    def isThereAnyBundleComplete(self):
        for index in range(0, len(Bundle.instances)):
            bundle = Bundle.instances[index]
            if ((bundle.closed == True) and
                    (bundle.bundleBinSizeX != 0.0) and
                    (bundle.bundleBinSizeY != 0.0)):
                return True
        return False

    def clearAgeMetallicityElements(self):
        # limpiar la tabla de rangos de edad y su variable
        self.ui.tableT3Age.model().removeRows(0, self.ui.tableT3Age.model().rowCount())
        self.ui.tableT3Age.setDisabled(True)
        self.ui.lineT3AgeRanges.clear()
        self.listT3AgeRanges = None
        # limpiar la tabla de rangos de metalicidad y su variable
        self.ui.tableT3Metallicity.model().removeRows(0, self.ui.tableT3Metallicity.model().rowCount())
        self.ui.tableT3Metallicity.setDisabled(True)
        self.ui.lineT3MetallicityRanges.clear()
        self.listT3MetallicityRanges = None
        # limpiar y deshabilitar los lineEdit de Age y Metallicity
        self.ui.lineT3AgeRanges.setDisabled(True)
        self.ui.lineT3MetallicityRanges.setDisabled(True)
        self.ui.lineT3AgeRanges.clear()
        self.ui.lineT3MetallicityRanges.clear()

    def clearOthers(self):
        self.ui.lineT3NumberOfBins.clear()
        self.ui.lineT3ZMin.clear()
        self.ui.lineT3ZMax.clear()
        self.ui.lineT3ZSun.clear()
        self.ui.checkboxT3RefineGrid.setChecked(False)
        self.ui.checkboxT3RefineAge.setChecked(False)
        self.ui.lineT3IntMother.clear()
        self.ui.lineT3AreaReg.clear()
        self.ui.lineT3BinningOffsetsX.clear()
        self.ui.lineT3BinningOffsetsY.clear()
        self.ui.lineT3DistRedd.clear()
        self.ui.lineT3DistReddCenterX.clear()
        self.ui.lineT3DistReddCenterY.clear()
        self.ui.lineT3NumberIterations.clear()
        self.ui.lineT3NumberProcessors.clear()

    @pyqtSlot()
    def buildHistograms(self):
        complete_bundles = list()
        for bundle in Bundle.instances:
            if ((bundle.closed == True) and
                    (bundle.bundleBinSizeX != 0.0) and
                    (bundle.bundleBinSizeY != 0.0)):
                complete_bundles.append(bundle)
        self.histogramT2Obs.clear()
        self.histogramT2Mod.clear()
        if len(complete_bundles) > 0:
            self.histogramT2Obs.draw(x_raw_data=self.observedDiagram.expressionDictionary['X'],
                                     y_raw_data=self.observedDiagram.expressionDictionary['Y'],
                                     bundles_instances=complete_bundles
                                     )
            self.histogramT2Mod.draw(x_raw_data=self.modelDiagram.expressionDictionary['X'],
                                     y_raw_data=self.modelDiagram.expressionDictionary['Y'],
                                     bundles_instances=complete_bundles
                                     )

    @pyqtSlot()
    def loadAgeRanges(self):
        strData = str(self.ui.lineT3AgeRanges.text())
        self.ui.tableT3Age.model().removeRows(0, self.ui.tableT3Age.model().rowCount())
        if len(strData) > 0:
            age_s = filter(None, re.split("[, \[\]]+", strData))
            self.listT3AgeRanges = [float(i) for i in age_s]
            if strictly_increasing(self.listT3AgeRanges):
                age_histogram = age_histo(self.modelDiagram.expressionDictionary['Age'], self.listT3AgeRanges)
                self.ui.tableT3Age.setEnabled(True)
                self.fillTable(self.ui.tableT3Age, self.listT3AgeRanges, age_histogram)
            else:
                self.ui.tableT3Age.setDisabled(True)
                self.dialog.customizeLabelErrorMessage(
                        'Error: The vector defining the age bins is not strictly increasing.')
                self.dialog.exec_()
                return
        else:
            self.listT3AgeRanges = None
            self.ui.tableT3Age.setDisabled(True)

    @pyqtSlot()
    def loadMetallicityRanges(self):
        strData = str(self.ui.lineT3MetallicityRanges.text())
        self.ui.tableT3Metallicity.model().removeRows(0, self.ui.tableT3Metallicity.model().rowCount())
        if len(strData) > 0:
            met_s = filter(None, re.split("[, \[\]]+", strData))
            self.listT3MetallicityRanges = [float(i) for i in met_s]
            if strictly_increasing(self.listT3MetallicityRanges):
                metallicity_histogram = met_histo(self.modelDiagram.expressionDictionary['Metallicity'],
                                                  self.listT3MetallicityRanges)
                self.ui.tableT3Metallicity.setEnabled(True)
                self.fillTable(self.ui.tableT3Metallicity, self.listT3MetallicityRanges, metallicity_histogram)
            else:
                self.ui.tableT3Metallicity.setDisabled(True)
                self.dialog.customizeLabelErrorMessage(
                        'Error: The vector defining the metallicity bins is not strictly increasing.')
                self.dialog.exec_()
                return
        else:
            self.listT3MetallicityRanges = None
            self.ui.tableT3Metallicity.setDisabled(True)

    def fillTable(self, table, values, histogram):
        for i in range(len(histogram)):
            item_lower = QTableWidgetItem(str(values[i]))
            item_upper = QTableWidgetItem(str(values[i + 1]))
            item_count = QTableWidgetItem(str(histogram[i]))
            flags = item_lower.flags()
            flags ^= Qt.ItemIsEditable
            item_lower.setFlags(flags)
            item_upper.setFlags(flags)
            item_count.setFlags(flags)
            table.insertRow(i)
            table.setItem(i, 0, item_lower)
            table.setItem(i, 1, item_upper)
            table.setItem(i, 2, item_count)

    @pyqtSlot()
    def setStateRefineGrid(self):
        if self.ui.checkboxT3RefineGrid.isChecked() == True:
            self.boolT3RefineGrid = True
        else:
            self.boolT3RefineGrid = False

    @pyqtSlot()
    def setStateRefineAge(self):
        if self.ui.checkboxT3RefineAge.isChecked() == True:
            self.boolT3RefineAge = True
        else:
            self.boolT3RefineAge = False

    @pyqtSlot()
    def loadIntMother(self):
        self.strT3IntMother = str(self.ui.lineT3IntMother.text())

    @pyqtSlot()
    def loadAreaReg(self):
        self.strT3AreaReg = str(self.ui.lineT3AreaReg.text())

    @pyqtSlot()
    def loadBinningOffsetsX(self):
        self.strT3BinningOffsetsX = str(self.ui.lineT3BinningOffsetsX.text())

    @pyqtSlot()
    def loadBinningOffsetsY(self):
        self.strT3BinningOffsetsY = str(self.ui.lineT3BinningOffsetsY.text())

    @pyqtSlot()
    def loadDistRedd(self):
        self.strT3DistRedd = str(self.ui.lineT3DistRedd.text())

    @pyqtSlot()
    def loadDistReddCenterX(self):
        self.strT3DistReddCenterX = str(self.ui.lineT3DistReddCenterX.text())

    @pyqtSlot()
    def loadDistReddCenterY(self):
        self.strT3DistReddCenterY = str(self.ui.lineT3DistReddCenterY.text())

    @pyqtSlot()
    def loadNumberOfBins(self):
        if len(self.ui.lineT3NumberOfBins.text()) > 0:
            self.intT3NumberOfBins = int(str(self.ui.lineT3NumberOfBins.text()))
            self.generateMetRanges()
        else:
            self.intT3NumberOfBins = 0
            self.ui.tableT3Metallicity.model().removeRows(0, self.ui.tableT3Metallicity.model().rowCount())
            self.ui.tableT3Metallicity.setDisabled(True)
            self.ui.lineT3MetallicityRanges.clear()

    @pyqtSlot()
    def loadZMin(self):
        if len(self.ui.lineT3ZMin.text()) > 0:
            self.floatT3ZMin = float(str(self.ui.lineT3ZMin.text()))
            self.generateMetRanges()
        else:
            self.floatT3ZMin = 0.0
            self.ui.tableT3Metallicity.model().removeRows(0, self.ui.tableT3Metallicity.model().rowCount())
            self.ui.tableT3Metallicity.setDisabled(True)
            self.ui.lineT3MetallicityRanges.clear()

    @pyqtSlot()
    def loadZMax(self):
        if len(self.ui.lineT3ZMax.text()) > 0:
            self.floatT3ZMax = float(str(self.ui.lineT3ZMax.text()))
            self.generateMetRanges()
        else:
            self.floatT3ZMax = 0.0
            self.ui.tableT3Metallicity.model().removeRows(0, self.ui.tableT3Metallicity.model().rowCount())
            self.ui.tableT3Metallicity.setDisabled(True)
            self.ui.lineT3MetallicityRanges.clear()

    @pyqtSlot()
    def loadZSun(self):
        if len(self.ui.lineT3ZSun.text()) > 0:
            self.floatT3ZSun = float(str(self.ui.lineT3ZSun.text()))
            self.generateMetRanges()
        else:
            self.floatT3ZSun = 0.0
            self.ui.tableT3Metallicity.model().removeRows(0, self.ui.tableT3Metallicity.model().rowCount())
            self.ui.tableT3Metallicity.setDisabled(True)
            self.ui.lineT3MetallicityRanges.clear()

    def generateMetRanges(self):
        if self.ui.lineT3MetallicityRanges.isEnabled():
            if ((self.intT3NumberOfBins > 0) and (self.floatT3ZMin > 0.0) and
                (self.floatT3ZMax > 0.0) and (self.floatT3ZSun > 0.0)):

                # equidistant bins in linear scale
                f = (self.intT3NumberOfBins - 1) / (self.floatT3ZMax - self.floatT3ZMin)
                met_lin = numpy.arange(self.intT3NumberOfBins) / f + self.floatT3ZMin

                # equidistant bins in log scale
                f = (self.intT3NumberOfBins - 1) / (
                    numpy.log10(self.floatT3ZMax / self.floatT3ZSun) - numpy.log10(self.floatT3ZMin / self.floatT3ZSun))
                met_log = numpy.arange(self.intT3NumberOfBins) / f + numpy.log10(self.floatT3ZMin / self.floatT3ZSun)

                # intermediate:
                met = self.floatT3ZSun * 10 ** numpy.mean([[numpy.log10(met_lin / self.floatT3ZSun)], [met_log]], 0)

                listValues = list()
                for value in met[0]:
                    value = round(value, C.NUM_DECIMAL_DIGITS_METALLICITY_RANGES)
                    listValues.append(value)

                self.ui.lineT3MetallicityRanges.clear()
                self.ui.lineT3MetallicityRanges.setText(str(listValues).strip('[]'))
                self.loadMetallicityRanges()

    @pyqtSlot()
    def loadNumberIterations(self):
        self.strT3NumberIterations = str(self.ui.lineT3NumberIterations.text())

    @pyqtSlot()
    def loadNumberProcessors(self):
        self.strT3NumberProcessors = str(self.ui.lineT3NumberProcessors.text())

    @pyqtSlot()
    def about_this(self):
        self.about.exec_()

    @pyqtSlot()
    def about_qt(self):
        self.q_application.aboutQt()


def main():
    app = QApplication(sys.argv)
    window = MainWindow(app)
    window.showMaximized()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()

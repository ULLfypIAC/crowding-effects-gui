from PyQt4.QtCore import pyqtSlot, QUrl
from PyQt4.QtGui import QDialog, QDesktopServices
from PyQt4.QtWebKit import QWebPage

from sources.resources.about import Ui_Dialog


class AboutThis(QDialog):
    def __init__(self):
        super(AboutThis, self).__init__()

        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.buttonBox.rejected.connect(self.closeDialog)
        self.ui.webView.page().setLinkDelegationPolicy(QWebPage.DelegateAllLinks)
        self.ui.webView.linkClicked.connect(self.open_link)
        self.ui.commandLinkButtonULL.clicked.connect(self.open_link_ull)
        self.ui.commandLinkButtonIAC.clicked.connect(self.open_link_iac)
        self.ui.commandLinkButtonPyQt.clicked.connect(self.open_link_pyqt)
        self.ui.commandLinkButtonMatplotlib.clicked.connect(self.open_link_matplotlib)

    @pyqtSlot(QUrl)
    def open_link(self, url):
        QDesktopServices.openUrl(url)

    @pyqtSlot()
    def closeDialog(self):
        self.close()

    @pyqtSlot(QUrl)
    def open_link_ull(self):
        self.open_link(QUrl('http://www.ull.es/'))

    @pyqtSlot(QUrl)
    def open_link_iac(self):
        self.open_link(QUrl('http://www.iac.es/'))

    @pyqtSlot(QUrl)
    def open_link_pyqt(self):
        self.open_link(QUrl('https://riverbankcomputing.com/software/pyqt/intro'))

    @pyqtSlot(QUrl)
    def open_link_matplotlib(self):
        self.open_link(QUrl('http://matplotlib.org/'))

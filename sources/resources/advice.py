# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/anthony/Projects/crowding-effects-gui/sources/resources/advice.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_adviceDialog(object):
    def setupUi(self, adviceDialog):
        adviceDialog.setObjectName(_fromUtf8("adviceDialog"))
        adviceDialog.resize(554, 110)
        self.gridLayout = QtGui.QGridLayout(adviceDialog)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.labelErrorMessage = QtGui.QLabel(adviceDialog)
        self.labelErrorMessage.setText(_fromUtf8(""))
        self.labelErrorMessage.setIndent(10)
        self.labelErrorMessage.setObjectName(_fromUtf8("labelErrorMessage"))
        self.verticalLayout.addWidget(self.labelErrorMessage)
        self.widget = QtGui.QWidget(adviceDialog)
        self.widget.setObjectName(_fromUtf8("widget"))
        self.gridLayout_2 = QtGui.QGridLayout(self.widget)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.pushButton = QtGui.QPushButton(self.widget)
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.horizontalLayout.addWidget(self.pushButton)
        self.gridLayout_2.addLayout(self.horizontalLayout, 0, 1, 1, 1)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem, 0, 0, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem1, 0, 2, 1, 1)
        self.verticalLayout.addWidget(self.widget)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(adviceDialog)
        QtCore.QMetaObject.connectSlotsByName(adviceDialog)

    def retranslateUi(self, adviceDialog):
        adviceDialog.setWindowTitle(_translate("adviceDialog", "Error", None))
        self.pushButton.setText(_translate("adviceDialog", "OK", None))


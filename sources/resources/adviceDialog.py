# -*- coding: utf-8 -*-

import sys

from PyQt4.QtGui import QDialog

from sources.resources.advice import Ui_adviceDialog


class AdviceDialog(QDialog):

	def __init__(self):
		super(AdviceDialog, self).__init__()

		self.ui = Ui_adviceDialog()
		self.ui.setupUi(self)

		self.ui.pushButton.clicked.connect(self.closeDialog)

	def customizeLabelErrorMessage(self, message):
		self.ui.labelErrorMessage.setText(message)

	def closeDialog(self):
		self.close()

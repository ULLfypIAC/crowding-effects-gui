#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

from PyQt4 import uic


def compile_qrc_resources(path):
    os.chdir(path)
    for file_with_extension in os.listdir(path):
        if file_with_extension.endswith(".qrc"):
            file_without_extension = file_with_extension.split('.')[-2]
            command = 'pyrcc4 -py2 -o ' + file_without_extension + '_rc.py ' + file_with_extension
            os.system(command)


def compile_ui_resources(path):
    os.chdir(path)
    uic.compileUiDir(dir=path)


def compile_all_resources(path):
    os.chdir(path)
    os.chdir('sources')
    os.chdir('resources')
    res_path = os.getcwd()
    compile_qrc_resources(res_path)
    compile_ui_resources(res_path)


def remove_qrc_resources(path):
    os.chdir(path)
    for file_with_extension in os.listdir(path):
        if file_with_extension.endswith(".qrc"):
            file_without_extension = file_with_extension.split('.')[-2]
            file_without_extension = path + file_without_extension + '_rc.py'
            if os.path.exists(file_without_extension):
                os.remove(file_without_extension)


def remove_ui_resources(path):
    os.chdir(path)
    for file_with_extension in os.listdir(path):
        if file_with_extension.endswith(".ui"):
            file_without_extension = file_with_extension.split('.')[-2]
            file_without_extension = path + file_without_extension + '.py'
            if os.path.exists(file_without_extension):
                os.remove(file_without_extension)


def remove_all_resources(path):
    os.chdir(path)
    os.chdir('sources')
    os.chdir('resources')
    res_path = os.getcwd()
    for file_with_extension in os.listdir(res_path):
        if file_with_extension.endswith(".pyc"):
            os.remove(file_with_extension)
    remove_qrc_resources(res_path)
    remove_ui_resources(res_path)

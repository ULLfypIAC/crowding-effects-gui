#!/usr/bin/python
# -*- coding: utf-8 -*-

from sources.constant import ApplicationConsts

ApplicationConsts.MIN_NUM_VERTEX_TO_CLOSE_BUNDLE = 3
ApplicationConsts.NUM_DECIMAL_DIGITS_POINTS = 5
ApplicationConsts.NUM_DECIMAL_DIGITS_METALLICITY_RANGES = 8
ApplicationConsts.NUM_DECIMAL_DIGITS_BUNDLES = 2

ApplicationConsts.LEFT_CLICK = 1
ApplicationConsts.RIGHT_CLICK = 3

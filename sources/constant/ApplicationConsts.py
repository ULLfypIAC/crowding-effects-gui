#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# modulo que implementa una clase
# para definir Constantes

import sys


class ApplicationConsts:
    class ApplicationConstsError(TypeError):
        pass

    def __setattr__(self, name, value):
        if self.__dict__.has_key(name):
            raise self.ApplicationConstsError, "Can't rebind const (%s)" % name

        self.__dict__[name] = value

    def __delattr__(self, name):
        if self.__dict__.has_key(name):
            raise self.ApplicationConstsError, "Can't unbind const (%s)" % name

        raise NameError, name


sys.modules[__name__] = ApplicationConsts()

#!/usr/bin/python
# -*- coding: utf-8 -*-

from sources.constant import Const

# *********************otras definiciones*********************#
Const.TAB_SIZE = 4

Const.FIN = "Final de fichero"

Const.ILLEGAL_CHARACTER = "Caracter ilegal"
Const.ILLEGAL_FLOAT = "Error de Numero Flotante"
# *********************otras definiciones*********************#

# **********definicion de constantes para caracteres**********#
Const.c_PLUS = '+'
Const.c_MINUS = '-'
Const.c_ASTERISK = '*'
Const.c_DIVIDE = '/'
Const.c_RIGHTPARENTHESIS = ')'
Const.c_LEFTPARENTHESIS = '('
Const.c_DOT = '.'
Const.c_E_UPPER = 'E'
Const.c_E_LOWER = 'e'
Const.c_C_LOWER = 'c'
Const.c_SPACE = ' '
Const.c_NEWLINE = '\n'
Const.c_TAB = '\t'
Const.c_CARRIAGE_RETURN = '\r'
# **********definicion de constantes para caracteres**********#

# ****definicion de constantes para los tokens o terminales***#
Const.t_PLUS = "PLUS"
Const.t_MINUS = "MINUS"
Const.t_ASTERISK = "ASTERISK"
Const.t_DIVIDE = "DIVIDE"
Const.t_RIGHTPARENTHESIS = "RIGHTPARENTHESIS"
Const.t_LEFTPARENTHESIS = "LEFTPARENTHESIS"
Const.t_ID = "ID"
Const.t_NUM = "NUM"
Const.t_FLOAT = "FLOAT"
Const.t_TOKEN_ERROR = "TOKEN_ERROR"
Const.t_ENDTEXT = "ENDTEXT"
# ****definicion de constantes para los tokens o terminales***#

# ***************definicion de conjuntos first****************#
Const.first_EXPR = set([Const.t_LEFTPARENTHESIS, Const.t_MINUS, Const.t_ID, Const.t_NUM, Const.t_FLOAT])
Const.first_EXPR_AUX = set([Const.t_PLUS, Const.t_MINUS])
Const.first_TERM = set([Const.t_LEFTPARENTHESIS, Const.t_MINUS, Const.t_ID, Const.t_NUM, Const.t_FLOAT])
Const.first_TERM_AUX = set([Const.t_ASTERISK, Const.t_DIVIDE])
Const.first_FACTOR = set([Const.t_LEFTPARENTHESIS, Const.t_MINUS, Const.t_ID, Const.t_NUM, Const.t_FLOAT])
# ***************definicion de conjuntos first****************#

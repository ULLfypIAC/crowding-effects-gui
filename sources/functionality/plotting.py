# -*- coding: utf-8 -*-

import matplotlib.pyplot as pyplot
from PyQt4 import QtCore
from PyQt4.QtCore import pyqtSignal, pyqtSlot
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT
from matplotlib.lines import Line2D
from sympy.geometry import Point, Segment, intersection

from sources.constant.ApplicationConstants import ApplicationConsts as C


class Bundle(QtCore.QObject):
    instances = list()  # Instances of Bundle objects
    instances_count = 0
    min_points = C.MIN_NUM_VERTEX_TO_CLOSE_BUNDLE

    """ Esta señal permitirá comprobrar el estado del bundle.
    Deberá ser emitida cuando un objeto instanciado de esta clase modifique
    alguno de estos atributos:
    - closed.
    - len.
    """
    signal_status = pyqtSignal(object, name='status')

    @classmethod
    def __new__(cls, *args, **kwargs):
        return super(Bundle, cls).__new__(cls, args, kwargs)

    @staticmethod
    def create():
        bundle = Bundle(x_points=list(),
                        y_points=list(),
                        closed=False,
                        name='B%02d' % (Bundle.instances_count + 1))
        Bundle.instances.append(bundle)
        Bundle.instances_count += 1
        return bundle

    @staticmethod
    def remove(index):
        bundle = None
        success = False
        try:
            bundle = Bundle.instances[index]
            del Bundle.instances[index]
            success = True
        finally:
            return bundle, success

    @staticmethod
    def remove_bundles():
        for index in range(len(Bundle.instances)):
            Bundle.remove(0)
            Diagram.line_remove(0)
        Bundle.instances_count = 0
        Diagram.clear_highlight()
        Diagram.update()

    @staticmethod
    def find(index):
        bundle = None
        success = False
        try:
            bundle = Bundle.instances[index]
            Diagram.selected_bundle({
                'instance': bundle,
                'index': index
            })
            bundle.signal_status.emit(bundle)
            success = True
        finally:
            return bundle, success

    @staticmethod
    def is_all_closed():
        closed_list = [bundle.closed for bundle in Bundle.instances]
        success = False not in closed_list
        del closed_list
        return success

    def __init__(self,
                 x_points=list(),
                 y_points=list(),
                 closed=False,
                 name='',
                 size=0):
        super(Bundle, self).__init__()
        self.x_points = x_points
        self.y_points = y_points
        self.closed = closed
        self.name = name
        self.len = size
        self.bundleBinSizeX = 0.0
        self.bundleBinSizeY = 0.0
        return

    def open(self):
        success = False
        try:
            # Extraer último punto
            self.x_points.pop(-1)
            self.y_points.pop(-1)
            self.closed = False
            # Emitir señal para evaluar nuevo estado
            self.signal_status.emit(self)
            success = True
        finally:
            return success

    def close(self):
        success = False
        try:
            # Añadir una copia del 1º punto al final
            self.x_points.append(self.x_points[0])
            self.y_points.append(self.y_points[0])
            self.closed = True
            # Emitir señal para evaluar nuevo estado
            self.signal_status.emit(self)
            success = True
        finally:
            return success

    def point_append(self, xdata, ydata):
        self.x_points.append(xdata)
        self.y_points.append(ydata)
        self.len = len(self.x_points)
        # Emitir señal para evaluar nuevo estado
        if self.len >= self.min_points:
            self.signal_status.emit(self)

    def point_modify(self, row, xdata, ydata):
        success = False
        try:
            self.x_points[row] = xdata
            self.y_points[row] = ydata
            # Comprobar si es necesario modificar el último elemento
            if row is 0 and self.closed:
                self.x_points[-1] = xdata
                self.y_points[-1] = ydata
            success = True
        finally:
            return success

    def point_remove(self, row):
        success = False
        try:
            del self.x_points[row]
            del self.y_points[row]
            self.len -= 1
            # Comprobar si el bundle está cerrado
            if self.closed is True:
                # Comprobar cantidad de punto definidos
                if self.len < Bundle.min_points:
                    # Reabrir bundle y quitar punto de cierre de bundle
                    self.closed = False
                    del self.x_points[-1]
                    del self.y_points[-1]
                elif row is 0:
                    # Redefinir punto de cierre de bundle
                    self.x_points[-1] = self.x_points[0]
                    self.y_points[-1] = self.y_points[0]
            # Emitir señal para evaluar nuevo estado
            self.signal_status.emit(self)
            success = True
        finally:
            return success


class NavigationToolbar(NavigationToolbar2QT):
    # list of toolitems to add to the toolbar, format is:
    # (
    #   text, # the text of the button (often not visible to users)
    #   tooltip_text, # the tooltip shown on hover (where possible)
    #   image_file, # name of the image for the button (without the extension)
    #   name_of_method, # name of the method in NavigationToolbar2 to call
    # )
    toolitems = (
        ('Home', 'Reset original view', 'home', 'home'),
        ('Back', 'Back to  previous view', 'back', 'back'),
        ('Forward', 'Forward to next view', 'forward', 'forward'),
        (None, None, None, None),
        ('Pan', 'Pan axes with left mouse, zoom with right', 'move', 'pan'),
        ('Zoom', 'Zoom to rectangle', 'zoom_to_rect', 'zoom'),
        ('Select', 'Select a point', 'hand', 'select'),
        (None, None, None, None),
        # ('Subplots', 'Configure subplots', 'subplots', 'configure_subplots'),
        ('Save', 'Save the figure', 'filesave', 'save_figure'),
        # (None, None, None, None),
    )

    def __init__(self, canvas, parent, diagram, coordinates=True):
        super(NavigationToolbar, self).__init__(canvas, parent, coordinates)
        self.diagram = diagram
        self.connections = list()
        return

    def _init_toolbar(self):
        super(NavigationToolbar, self)._init_toolbar()
        self.zoomAction = self._actions['zoom']
        self.panAction = self._actions['pan']
        self.selectAction = self._actions['select']
        self.selectAction.setCheckable(True)
        # Eliminar la acción de modificar dimensiones
        action = self.actions()[-2]
        self.removeAction(action)

    def _update_buttons_checked(self):
        super(NavigationToolbar, self)._update_buttons_checked()
        self.selectAction.setChecked(self._active == 'SELECT')

    @pyqtSlot(tuple)
    def select(self, *args):
        # Modificar atributo self._active
        if self._active == 'SELECT':
            self._active = None
        else:
            self._active = 'SELECT'
        # Modificar atributo self.__idPress
        if self._idPress is not None:
            self._idPress = self.canvas.mpl_disconnect(self._idPress)
            self.mode = ''
        # Modificar atributo self.__idRelease
        if self._idRelease is not None:
            self._idRelease = self.canvas.mpl_disconnect(self._idRelease)
            self.mode = ''
        # Borrar puntos resaltados
        Diagram.point_highlight([], [], False)
        # Comprobar si está activo la action de la toolbar
        if self._active:
            # Establecer cursor al entrar y salir del eje
            cid = self.canvas.mpl_connect(
                    'axes_enter_event',
                    self.select_axes_enter_event
            )
            self.connections.append(cid)
            cid = self.canvas.mpl_connect(
                    'axes_leave_event',
                    self.select_axes_leave_event
            )
            self.connections.append(cid)
            self._idPress = self.canvas.mpl_connect(
                    'pick_event',
                    self.select_pick_event
            )
            self.mode = 'select point'
        else:
            for cid in self.connections:
                self.canvas.mpl_disconnect(cid)
            del self.connections[:]
        for axe in self.canvas.figure.get_axes():
            axe.set_navigate_mode(self._active)
        self.set_message(self.mode)
        self._update_buttons_checked()

    @pyqtSlot()
    def select_axes_enter_event(self, event):
        self.canvas.setCursor(QtCore.Qt.PointingHandCursor)

    @pyqtSlot()
    def select_axes_leave_event(self, event):
        self.canvas.setCursor(QtCore.Qt.ArrowCursor)

    @pyqtSlot()
    def select_pick_event(self, event):
        point_captured = Point(event.mouseevent.xdata, event.mouseevent.ydata)
        index_a = event.ind[0]
        index_b = index_a + 1
        line = event.artist
        x_points, y_points = line.get_data()
        if index_b == len(x_points):
            index_a = -2
            index_b = -1
        point_a = Point(x_points[index_a], y_points[index_a])
        point_b = Point(x_points[index_b], y_points[index_b])
        list_points = [point_a, point_b]
        list_distance = [point_a.distance(point_captured), point_b.distance(point_captured)]
        index_distance = list_distance.index(min(list_distance))
        flag = False
        xdata = 0.0
        ydata = 0.0
        if list_distance[index_distance] < 0.1:
            xdata = list_points[index_distance].x
            ydata = list_points[index_distance].y
            flag = True
        else:
            seg = Segment(point_a, point_b)
            seg1 = seg.perpendicular_segment(point_captured)
            intersection_list = intersection(seg, seg1)
            if intersection_list:
                xdata = intersection_list[0].x
                ydata = intersection_list[0].y
                flag = True
        bundle = self.diagram.selected_bundle.get('instance', None)
        # Comprobar si el bundle existe y está abierto
        if bundle is not None and bundle.closed is False and flag:
            # El bundle existe y está abierto
            # Emitir señal para añadir punto existente
            self.diagram.signal_canvas_press.emit({
                'button': 1,
                'xdata': xdata,
                'ydata': ydata
            })
            Diagram.point_highlight([xdata], [ydata], True)
        self._update_buttons_checked()
        # Evitar el doble disparo de la señal 'pick_event',
        # puesto que en el fichero artist.py de matplotlib
        # en el método pick, lanza la señal para él (Pick self)
        # y para sus hijos (Pick children).
        self.select(tuple())


class Diagram(QtCore.QObject):
    instances = list()
    signal_canvas_press = pyqtSignal(dict, name='canvasPress')

    @classmethod
    def __new__(cls, *args, **kwargs):
        return super(Diagram, cls).__new__(cls, args, kwargs)

    @staticmethod
    def create(parent):
        diagram = Diagram(parent)
        Diagram.instances.append(diagram)
        return diagram

    @staticmethod
    def update():
        for diagram in Diagram.instances:
            diagram.canvas.draw()

    @staticmethod
    def line_create():
        line = None
        success = False
        # Comprobar las instancias de la clase Diagram
        if not Diagram.instances:
            return line, success
        # Añadir una nueva línea en cada instancia de la clase Diagram
        for diagram in Diagram.instances:
            line = Line2D(
                    [],  # x points
                    [],  # y points
                    linestyle='-',  #
                    color='#FFFFFF',  #
                    marker='.',  #
                    markeredgewidth=0,  #
                    markeredgecolor='#FFFFFF',  #
                    picker=5
            )
            diagram.lines.append(line)
            diagram.axe.add_line(line)
        success = True
        return line, success

    @staticmethod
    def line_remove(index):
        line = None
        success = False
        # Comprobar las instancias de la clase Diagram
        if not Diagram.instances:
            return line, success
        if index < 0:
            return line, success
        try:
            for diagram in Diagram.instances:
                line = diagram.lines[index]
                diagram.axe.lines.remove(line)
                del diagram.lines[index]
            success = True
        finally:
            Diagram.update()
            return line, success

    @staticmethod
    def line_edit(index, x_points, y_points):
        line = None
        success = False
        # Comprobar las instancias de la clase Diagram
        if not Diagram.instances:
            return line, success
        if index < 0:
            return line, success
        try:
            for diagram in Diagram.instances:
                # Obtener puntos de la línea actual
                line = diagram.lines[index]
                # Actualizar datos
                line.set_data(x_points, y_points)
            success = True
        finally:
            Diagram.update()
            return line, success

    @staticmethod
    def clear_highlight():
        line = None
        success = False
        # Comprobar las instancias de la clase Diagram
        if not Diagram.instances:
            return line, success
        # Añadir una nueva línea en cada instancia de la clase Diagram
        for diagram in Diagram.instances:
            diagram.selected_points.set_visible(False)
            diagram.selected_points.set_data([], [])
        success = True
        return line, success

    @staticmethod
    def point_highlight(x_points, y_points, visible=True):
        success = False
        # Comprobar las instancias de la clase Diagram
        if not Diagram.instances:
            return success
        try:
            for diagram in Diagram.instances:
                diagram.selected_points.set_visible(visible)
                diagram.selected_points.set_data(x_points, y_points)
            success = True
        finally:
            Diagram.update()
            return success

    @staticmethod
    def canvas_connect():
        for diagram in Diagram.instances:
            if diagram.connection is None:
                diagram.canvas_operable(checked=False)

    @staticmethod
    def canvas_disconnect():
        for diagram in Diagram.instances:
            if diagram.connection is not None:
                diagram.canvas.mpl_disconnect(diagram.connection)
                diagram.connection = None
                diagram.canvas.setCursor(QtCore.Qt.ArrowCursor)

    @staticmethod
    def selected_bundle(dictionary):
        for diagram in Diagram.instances:
            diagram.selected_bundle = dictionary

    def __init__(self, parent):
        super(Diagram, self).__init__(parent)
        self.figure = pyplot.figure()
        self.axe = pyplot.axes()
        self.lines = list()
        self.canvas = FigureCanvas(self.figure)
        self.figure.set_canvas(self.canvas)
        self.toolbar = NavigationToolbar(canvas=self.canvas,
                                         parent=parent,
                                         diagram=self,
                                         coordinates=True)
        self.colorbar = None
        self.image = None
        self.colorBarActivada = 0
        self.connection = None
        self.selected_bundle = dict()
        self.selected_points, = self.axe.plot([],
                                              [],
                                              color=None,
                                              marker='o',
                                              markeredgecolor='#000000',
                                              markerfacecolor='#FF0BFF',
                                              markersize=10,
                                              alpha=0.5,
                                              picker=0,
                                              visible=False)
        self.toolbar.zoomAction.toggled.connect(self.canvas_operable)
        self.toolbar.panAction.toggled.connect(self.canvas_operable)
        self.toolbar.selectAction.toggled.connect(self.canvas_operable)
        return

    def clear_plot(self):
        # Clearing image
        # self.axe.cla()
        # Restoring axis limits
        self.axe.set_ylim((0, 1))
        self.axe.set_xlim((0, 1))
        self.axe.set_xlabel('')
        self.axe.set_ylabel('')
        # Clear color bar
        if self.colorBarActivada == 1:
            self.colorbar.remove()
            self.colorBarActivada = 0
        self.canvas.draw()

    @pyqtSlot(bool)
    def canvas_operable(self, checked):
        # Comprobar si algún botón de la toolbar está pulsado
        checked |= self.toolbar.zoomAction.isChecked()
        checked |= self.toolbar.panAction.isChecked()
        checked |= self.toolbar.selectAction.isChecked()
        if checked:
            # Algún botón de la toolbar está pulsado
            if self.connection is not None:
                # La señal 'button_press_event' sí está conectada al canvas
                self.canvas.mpl_disconnect(self.connection)
                self.connection = None
                # No cambiar el cursor para que se muestre el que determina
                # la acción seleccionada en el toobar
                # self.canvas.setCursor(QtCore.Qt.ArrowCursor)
        else:
            # Ningún botón de la toolbar está pulsado
            bundle = self.selected_bundle.get('instance', None)
            if self.connection is None \
                    and bundle is not None \
                    and bundle.closed is False:
                # La señal 'button_press_event' no está conectada al canvas
                self.connection = self.canvas.mpl_connect(
                        'button_press_event',
                        self.canvas_press
                )
                self.canvas.setCursor(QtCore.Qt.CrossCursor)

    @pyqtSlot()
    # @pyqtSlot('matplotlib.backend_bases.MouseEvent')
    def canvas_press(self, event):
        # Ignorar clicks fuera del eje
        if event.inaxes != self.axe:
            return
        # Emitir señal
        self.signal_canvas_press.emit({
            'button': event.button,
            'xdata': event.xdata,
            'ydata': event.ydata
        })

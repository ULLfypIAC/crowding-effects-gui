# -*- coding: utf-8 -*-


def generateScriptFile(
        scriptFileName,
        obscmd,
        modcmd,
        ran_c,
        ran_m,
        bundles,
        age,
        met,
        intMother,
        areaReg,
        refineGrid,
        binningOffsets,
        numberIterations,
        numberProcessors
):
    scriptFile = open(scriptFileName, 'w')

    scriptFile.write("# Observed CMD\n")
    if obscmd == None:
        scriptFile.write("obscmd = None\n\n")
    else:
        scriptFile.write("obscmd = '")
        scriptFile.write(str(obscmd))
        scriptFile.write("'\n\n")

    scriptFile.write("# Model CMD\n")
    if modcmd == None:
        scriptFile.write("modcmd = None\n\n")
    else:
        scriptFile.write("modcmd = '")
        scriptFile.write(str(modcmd))
        scriptFile.write("'\n\n")

    scriptFile.write("# CMD color and magnitude ranges\n")
    scriptFile.write("ran_c = ")
    scriptFile.write(str(ran_c))
    scriptFile.write("\n")
    scriptFile.write("ran_m = ")
    scriptFile.write(str(ran_m))
    scriptFile.write("\n\n")

    scriptFile.write("# Bundles (one line per bundle: b=[[x_vertices], [y_vertices]])\n")
    for bundle in bundles:
        scriptFile.write("b = [")
        scriptFile.write(str(bundle.x_points))
        scriptFile.write(", ")
        scriptFile.write(str(bundle.y_points))
        scriptFile.write("]\n")
    scriptFile.write("\n")
    if bundles == []:
        scriptFile.write("\n")

    scriptFile.write("# Bin sizes in color and magnitude (one line per bundle)\n")
    for bundle in bundles:
        scriptFile.write("bin_cm = [")
        if bundle.bundleBinSizeX == False:
            scriptFile.write("0.00")
        else:
            scriptFile.write(str(bundle.bundleBinSizeX))
        scriptFile.write(", ")
        if bundle.bundleBinSizeY == False:
            scriptFile.write("0.00")
        else:
            scriptFile.write(str(bundle.bundleBinSizeY))
        scriptFile.write("]\n")
    scriptFile.write("\n")
    if bundles == []:
        scriptFile.write("\n")

    scriptFile.write("# Age (in Gyr) and metallicity (in Z) bins\n")
    scriptFile.write("age = ")
    scriptFile.write(str(age))
    scriptFile.write("\n")
    scriptFile.write("met = ")
    scriptFile.write(str(met))
    scriptFile.write("\n\n")
    scriptFile.write("# Normalizing constants (intmother in M_sun; areareg in pc^2)\n")
    scriptFile.write("intmother = ")
    scriptFile.write(str(intMother))
    scriptFile.write("\n")
    scriptFile.write("areareg = ")
    scriptFile.write(str(areaReg))
    scriptFile.write("\n\n")
    scriptFile.write("# Refine the grid to find the minimum in the chi2 map?\n")
    scriptFile.write("refine_grid = ")
    scriptFile.write(str(refineGrid))
    scriptFile.write("\n\n")
    scriptFile.write("# Number of binning offsets in color and magnitude\n")
    scriptFile.write("shifts = ")
    scriptFile.write(str(binningOffsets))
    scriptFile.write("\n\n")
    scriptFile.write("# Number of iterations of Poissonian resampling of the observed CMD\n")
    scriptFile.write("nPoisson = ")
    scriptFile.write(str(numberIterations))
    scriptFile.write("\n\n")
    scriptFile.write("# Number of processors to use when running in parallel\n")
    scriptFile.write("nProcessors = ")
    scriptFile.write(str(numberProcessors))
    scriptFile.write("\n\n")
    scriptFile.close()

# -*- coding: utf-8 -*-

import os
import re

import numexpr
import pyfits
from sympy import var, Symbol

from sources.analizer.parser import Syntax
from sources.legacy.readcol import readcol


class ManageDiagram():
    def __init__(self):
        self.stdPythonPathToFile = ''
        self.filename = ''
        self.filenameExtension = ''
        self.data = None
        self.dataRows = None
        self.dataColumns = None
        self.columnNames = None
        self.dataDictionary = None
        self.expressionDictionary = None

    def setNames(self, path):
        self.stdPythonPathToFile = str(path)
        self.filename = os.path.basename(self.stdPythonPathToFile)
        __, self.filenameExtension = os.path.splitext(self.filename)

    def readData(self):
        if (len(self.stdPythonPathToFile) > 0):
            self.dataDictionary = {}
            self.expressionDictionary = {}
            if (self.filenameExtension == '.fits'):
                self.readFitsFormat()
                self.dataColumns = len(self.data.columns)
                self.createColumnNames()
                self.createFitsDictionary()
            else:
                self.readReadColFormat()
                self.dataColumns = len(self.data[0])
                self.createColumnNames()
                self.createReadColDictionary()
            self.dataRows = len(self.data)
            return 1
        return -1

    def readFitsFormat(self):
        fileData = pyfits.open(self.stdPythonPathToFile)
        self.data = fileData[1].data
        fileData.close()

    def createFitsDictionary(self):
        for j in self.columnNames:
            exec ('{j} = Symbol("{j}")'.format(j=j))

        i = 0
        for j in self.columnNames:
            exec ('self.dataDictionary["{j}"] = self.data.field(i)'.format(j=j))
            i = i + 1

    def readReadColFormat(self):
        self.data = readcol(self.stdPythonPathToFile)

    def createReadColDictionary(self):
        for j in self.columnNames:
            exec ('{j} = Symbol("{j}")'.format(j=j))

        i = 0
        for j in self.columnNames:
            exec ('self.dataDictionary["{j}"] = self.data[:,i]'.format(j=j))
            i = i + 1

    def createColumnNames(self):
        self.columnNames = [var('c%d' % j) for j in range(1, self.dataColumns + 1)]

    def calculateExpression(self, strExpr, field):

        parser = Syntax(strExpr)
        parser.inicio()
        if parser.error == 1:
            return parser.error

        # comprobando que la expresion no contenga columnas inexistentes
        pattern = re.compile('c\d*')
        match = pattern.findall(strExpr)

        columnNamesStr = []
        for column in self.columnNames:
            columnNamesStr.append(str(column))

        for columnExp in match:
            if not (columnExp in columnNamesStr):
                return 2

        expression = numexpr.evaluate(strExpr, self.dataDictionary)
        self.expressionDictionary[field] = expression
        return 0

    def clearContents(self):
        self.stdPythonPathToFile = ''
        self.filename = ''
        self.filenameExtension = ''
        self.data = None
        self.dataRows = None
        self.dataColumns = None
        self.columnNames = None
        self.dataDictionary = None
        self.expressionDictionary = None

    def deleteKey(self, key):
        if key in self.expressionDictionary:
            del self.expressionDictionary[key]

    def printContents(self):
        print 'Ruta completa: ' + self.stdPythonPathToFile
        print 'Nombre del fichero: ' + self.filename
        print 'Extension: ' + self.filenameExtension
        print 'Data: '
        print self.data
        print 'Data Rows: ' + str(self.dataRows)
        print 'Data Columns: ' + str(self.dataColumns)
        print 'Column Names: ' + str(self.columnNames)
        print 'Data Dictionary: ' + str(self.dataDictionary)
        print 'Expression Dictionary: ' + str(self.expressionDictionary)

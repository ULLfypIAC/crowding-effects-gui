# -*- coding: utf-8 -*-
"""

"""

import matplotlib
import numpy
from PyQt4.QtCore import QObject, pyqtSlot
from matplotlib import pyplot, path
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT

from sources.functionality.plotting import Bundle


class NavigationToolbar(NavigationToolbar2QT):
    # list of toolitems to add to the toolbar, format is:
    # (
    #   text, # the text of the button (often not visible to users)
    #   tooltip_text, # the tooltip shown on hover (where possible)
    #   image_file, # name of the image for the button (without the extension)
    #   name_of_method, # name of the method in NavigationToolbar2 to call
    # )
    toolitems = (
        ('Home', 'Reset original view', 'home', 'home'),
        ('Back', 'Back to  previous view', 'back', 'back'),
        ('Forward', 'Forward to next view', 'forward', 'forward'),
        (None, None, None, None),
        ('Pan', 'Pan axes with left mouse, zoom with right', 'move', 'pan'),
        ('Zoom', 'Zoom to rectangle', 'zoom_to_rect', 'zoom'),
        (None, None, None, None),
        # ('Subplots', 'Configure subplots', 'subplots', 'configure_subplots'),
        ('Save', 'Save the figure', 'filesave', 'save_figure'),
        # (None, None, None, None),
    )

    def __init__(self, canvas, parent, coordinates=False):
        super(NavigationToolbar, self).__init__(canvas, parent, coordinates)
        return

    def _init_toolbar(self):
        super(NavigationToolbar, self)._init_toolbar()
        self.zoomAction = self._actions['zoom']
        self.panAction = self._actions['pan']
        # Eliminar la acción de modificar dimensiones asumiendo que coordinates=False
        action = self.actions()[-1]
        self.removeAction(action)
        pass


class Histogram(QObject):
    def __init__(self, parent=None):
        super(Histogram, self).__init__(parent)
        self.figure = pyplot.figure()
        self.axe = self.figure.add_subplot(1, 1, 1)
        self.canvas = FigureCanvasQTAgg(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, parent)
        self.figure.set_canvas(self.canvas)
        self.connect_id = None
        self.legend = None
        self.polygons = list()
        self.handles = dict()

    def clear(self):
        if self.connect_id is not None:
            self.canvas.mpl_disconnect(self.connect_id)
        self.handles.clear()
        if self.legend is not None:
            self.legend.remove()
            del self.legend
            self.legend = None
        self.axe.clear()
        self.canvas.draw()

    def draw(self, x_raw_data, y_raw_data, bundles_instances):
        bundles, bundles_names, ran_cm, bin_cm, nbin_cm, xypoints = self.prepare(x_raw_data, y_raw_data,
                                                                                 bundles_instances)
        histogram = self.multi_hist(bundles, ran_cm, bin_cm, nbin_cm, xypoints)
        self.plot_binhist(histogram, bundles, bundles_names)

    @staticmethod
    def prepare(x_raw_data, y_raw_data, bundles_instances):
        # Comprobar tipo de datos de las entradas
        assert isinstance(x_raw_data, numpy.ndarray), 'x_raw_data debe ser una instancia de numpy.ndarray'
        assert isinstance(y_raw_data, numpy.ndarray), 'y_raw_data debe ser una instancia de numpy.ndarray'
        assert x_raw_data.shape == y_raw_data.shape, 'x_raw_data.shape y y_raw_data.shape deben ser iguales'
        assert isinstance(bundles_instances, list), 'bundles_instances debe ser una instancia de list'
        # Calcular xypoints
        xypoints = numpy.array([x_raw_data, y_raw_data]).T
        # Calcular bundles y bin_cm
        bundles = list()
        bundles_names = list()
        x_bin = list()
        y_bin = list()
        for bundle in bundles_instances:
            assert isinstance(bundle, Bundle), 'bundle debe ser una instancia de Bundle'
            bundles.append(numpy.array([bundle.x_points, bundle.y_points]).T)
            bundles_names.append(bundle.name)
            x_bin.append(bundle.bundleBinSizeX)
            y_bin.append(bundle.bundleBinSizeY)
        bin_cm = numpy.array([x_bin, y_bin]).T
        # Calcular ran_cm y nbin_cm
        # Color and magnitude ranges for each bundle
        ran_cm = numpy.zeros([len(bundles), 4])
        for i, b in enumerate(bundles):
            ran_cm[i, :] = [min(b[:, 0]), max(b[:, 0]), min(b[:, 1]), max(b[:, 1])]
        # Number of color and magnitude bins per bundle
        nbin_cm = numpy.ceil(numpy.array([ran_cm[:, 1] - ran_cm[:, 0], ran_cm[:, 3] - ran_cm[:, 2]]).T / bin_cm)
        # Color and magnitude ranges - Correcting to get integer number of bins
        ran_cm[:, 1] = ran_cm[:, 0] + bin_cm[:, 0] * nbin_cm[:, 0]
        ran_cm[:, 3] = ran_cm[:, 2] + bin_cm[:, 1] * nbin_cm[:, 1]
        # Comprobar tipos de datos de las salidas
        assert isinstance(xypoints, numpy.ndarray), 'xypoint debe ser una instancia de numpy.ndarray'
        assert isinstance(ran_cm, numpy.ndarray), 'ran_cm debe ser una instancia de numpy.ndarray'
        assert isinstance(bin_cm, numpy.ndarray), 'bin_cm debe ser una instancia de numpy.ndarray'
        assert isinstance(nbin_cm, numpy.ndarray), 'nbin_cm debe ser una instancia de numpy.ndarray'
        assert isinstance(bundles, list), 'bundles debe ser una instancia de list'
        assert isinstance(bundles_names, list), 'bundles_names debe ser una instancia de list'
        for i, bundle in enumerate(bundles):
            assert isinstance(bundle, numpy.ndarray), 'bundle debe ser una instancia de numpy.array'
        for i, bundle in enumerate(bundles_names):
            assert isinstance(bundle, str), 'bundle debe ser una instancia de str'
        return bundles, bundles_names, ran_cm, bin_cm, nbin_cm, xypoints

    @staticmethod
    def multi_hist(bundles, ran_cm, bin_cm, nbin_cm, xypoints, offsets=[]):
        nb = len(bundles)
        arr = []
        for i in range(nb):
            if offsets:  # offsets != []
                offc = numpy.array([offsets[0], 1 + offsets[0]]) * bin_cm[i, 0]
                offm = numpy.array([offsets[1], 1 + offsets[1]]) * bin_cm[i, 1]
                colran = ran_cm[i, 0:2] + offc
                magran = ran_cm[i, 2:] + offm
                nbincm = nbin_cm[i, :] + 1
            else:
                colran = ran_cm[i, 0:2]
                magran = ran_cm[i, 2:]
                nbincm = nbin_cm[i, :]
            s = matplotlib.path.Path(bundles[i]).contains_points(xypoints)
            hist, xedg, yedg = numpy.histogram2d(
                    xypoints[s, 1],
                    xypoints[s, 0],
                    range=[magran, colran],
                    bins=nbincm
            )
            hist = hist.reshape(numpy.size(hist))
            arr.append(hist)
        # return concatenate(arr)
        return arr

    def plot_binhist(self, histogram, bundles, bundles_names):
        # Operaciones iniciales
        rr = numpy.array(range(9)) + 1
        bins = numpy.concatenate((rr, rr * 1e1, rr * 1e2, rr * 1e3, rr * 1e4, rr * 1e5, rr * 1e6))
        # Representar histograma
        del self.polygons[:]
        nb = len(bundles)
        for i in range(nb):
            #
            __, __, patches = self.axe.hist(
                    x=histogram[i],
                    bins=bins,
                    histtype='step',
                    label=bundles_names[i]
            )
            # Comprobar tipos y añadir polígonos a la lista
            assert isinstance(patches[0], matplotlib.patches.Polygon)
            self.polygons.append(patches[0])
        # Operaciones incluidas en la función
        self.axe.semilogx()
        self.axe.set_xlim(0.9, 200000.)
        # Crear la leyenda
        self.legend = self.axe.legend(
                loc='best',
                fancybox=True,
                shadow=True,
                handletextpad=0.2,
                columnspacing=0.5,
                ncol=2,
                framealpha=0.01
        )
        # Vincular figuras de la leyenda con las figuras originales
        self.handles.clear()
        for leg_polygon, orig_polygon in zip(self.legend.legendHandles, self.polygons):
            leg_polygon.set_picker(5)
            self.handles[leg_polygon] = orig_polygon
        # Conectar slot que manipula la activación y desactivación de los elementos
        self.connect_id = self.canvas.mpl_connect('pick_event', self.onpick_legend)
        # Refrescar visualización
        self.canvas.draw()

    @pyqtSlot()
    # @pyqtSlot('matplotlib.backend_bases.PickEvent')
    def onpick_legend(self, event):
        leg_polygon = event.artist
        orig_polygon = self.handles[leg_polygon]
        vis = not orig_polygon.get_visible()
        orig_polygon.set_visible(vis)
        # Change the alpha on the line in the legend so we can see what lines
        # have been toggled
        if vis:
            leg_polygon.set_alpha(1.0)
        else:
            leg_polygon.set_alpha(0.2)
        self.canvas.draw()

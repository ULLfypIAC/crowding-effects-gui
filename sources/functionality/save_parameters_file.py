# -*- coding: utf-8 -*-

def saveParametersFile(
        parametersFileName,
        obscmd,
        obsExpXaxis,
        obsExpYaxis,
        modcmd,
        modExpXaxis,
        modExpYaxis,
        modExpAge,
        modExpMetallicity,
        ran_c,
        ran_m,
        bundles,
        age,
        met,
        numberOfBins,
        zMin,
        zMax,
        zSun,
        intMother,
        areaReg,
        distRedd,
        distReddCenterX,
        distReddCenterY,
        refineGrid,
        binningOffsetsX,
        binningOffsetsY,
        refineAge,
        numberIterations,
        numberProcessors
):
    parametersFile = open(parametersFileName, 'w')

    parametersFile.write("# Observed CMD\n")
    if obscmd == None:
        parametersFile.write("obscmd = None\n")
    else:
        parametersFile.write("obscmd = '")
        parametersFile.write(str(obscmd))
        parametersFile.write("'\n")
    parametersFile.write("obsExpXaxis = ")
    parametersFile.write(str(obsExpXaxis))
    parametersFile.write("\n")
    parametersFile.write("obsExpYaxis = ")
    parametersFile.write(str(obsExpYaxis))
    parametersFile.write("\n\n")

    parametersFile.write("# Model CMD\n")
    if modcmd == None:
        parametersFile.write("modcmd = None\n")
    else:
        parametersFile.write("modcmd = '")
        parametersFile.write(str(modcmd))
        parametersFile.write("'\n")
    parametersFile.write("modExpXaxis = ")
    parametersFile.write(str(modExpXaxis))
    parametersFile.write("\n")
    parametersFile.write("modExpYaxis = ")
    parametersFile.write(str(modExpYaxis))
    parametersFile.write("\n")
    parametersFile.write("modExpAge = ")
    parametersFile.write(str(modExpAge))
    parametersFile.write("\n")
    parametersFile.write("modExpMetallicity = ")
    parametersFile.write(str(modExpMetallicity))
    parametersFile.write("\n\n")

    parametersFile.write("# CMD color and magnitude ranges\n")
    parametersFile.write("ran_c = ")
    if ran_c == None:
        parametersFile.write("[-0.49, 2.49]")
    else:
        parametersFile.write(str(ran_c))
    parametersFile.write("\n")

    parametersFile.write("ran_m = ")
    if ran_m == None:
        parametersFile.write("[4.99, -4.99]")
    else:
        parametersFile.write(str(ran_m))
    parametersFile.write("\n\n")

    parametersFile.write("# Bundles (one line per bundle: b=[[x_vertices], [y_vertices]])\n")
    for bundle in bundles:
        parametersFile.write("b = [")
        parametersFile.write(str(bundle.x_points))
        parametersFile.write(", ")
        parametersFile.write(str(bundle.y_points))
        parametersFile.write("]\n")
    parametersFile.write("\n")
    if bundles == []:
        parametersFile.write("\n")

    parametersFile.write("# Bin sizes in color and magnitude (one line per bundle)\n")
    for bundle in bundles:
        parametersFile.write("bin_cm = [")
        parametersFile.write(str(bundle.bundleBinSizeX))
        parametersFile.write(", ")
        parametersFile.write(str(bundle.bundleBinSizeY))
        parametersFile.write("]\n")
    parametersFile.write("\n")
    if bundles == []:
        parametersFile.write("\n")

    parametersFile.write("# Age (in Gyr) and metallicity (in Z) bins\n")
    parametersFile.write("age = ")
    if age == None:
        parametersFile.write("[0., 0.25, 0.5, 0.75, 1., 1.25, 1.5, 1.75, 2., 2.5, 3., 3.5, 4., 4.5, 5., 6., 7., 8., 9., 10., 11., 12., 13., 14., 15.]")
    else:
        parametersFile.write(str(age))
    parametersFile.write("\n")

    parametersFile.write("met = ")
    if met == None:
        parametersFile.write("[0.0004, 0.0007, 0.0011, 0.0016, 0.0022, 0.0029, 0.0037, 0.0046, 0.0056, 0.0067, 0.0079, 0.0092, 0.0106, 0.0122, 0.0140, 0.0160, 0.0182, 0.0206, 0.0232, 0.0260, 0.0290]")
    else:
        parametersFile.write(str(met))
    parametersFile.write("\n\n")

    parametersFile.write("# Parameters to calculate met bins\n")
    parametersFile.write("number_of_bins = ")
    if numberOfBins == None:
        parametersFile.write("12")
    else:
        parametersFile.write(str(numberOfBins))
    parametersFile.write("\n")

    parametersFile.write("z_min = ")
    if zMin == None:
        parametersFile.write("0.0001")
    else:
        parametersFile.write(str(zMin))
    parametersFile.write("\n")

    parametersFile.write("z_max = ")
    if zMax == None:
        parametersFile.write("0.002")
    else:
        parametersFile.write(str(zMax))
    parametersFile.write("\n")

    parametersFile.write("z_sun = ")
    if zSun == None:
        parametersFile.write("0.0198")
    else:
        parametersFile.write(str(zSun))
    parametersFile.write("\n\n")

    parametersFile.write("# Normalizing constants (intmother in M_sun; areareg in pc^2)\n")
    parametersFile.write("intmother = ")
    if intMother == None:
        parametersFile.write("1.")
    else:
        parametersFile.write(str(intMother))
    parametersFile.write("\n")

    parametersFile.write("areareg = ")
    if areaReg == None:
        parametersFile.write("1.")
    else:
        parametersFile.write(str(areaReg))
    parametersFile.write("\n\n")

    parametersFile.write("# Offset observed CMD in color & magnitude (i.e. ~ reddening & distance)...\n")
    parametersFile.write("# (S)earch minimum, check only (O)ne position, or sample full (G)rid?\n")
    parametersFile.write("dist_redd = '")
    if distRedd == None:
        parametersFile.write("S")
    else:
        parametersFile.write(str(distRedd))
    parametersFile.write("'\n\n")

    parametersFile.write("# ...centered on:\n")
    parametersFile.write("dist_redd_center = [")
    if distReddCenterX == None:
        parametersFile.write("0.0")
    else:
        parametersFile.write(str(distReddCenterX))
    parametersFile.write(", ")
    if distReddCenterY == None:
        parametersFile.write("0.0")
    else:
        parametersFile.write(str(distReddCenterY))
    parametersFile.write("]\n\n")

    parametersFile.write("# Refine the grid to find the minimum in the chi2 map?\n")
    parametersFile.write("refine_grid = ")
    if refineGrid == None:
        parametersFile.write("False")
    else:
        parametersFile.write(str(refineGrid))
    parametersFile.write("\n\n")

    parametersFile.write("# Number of binning offsets in color and magnitude\n")
    parametersFile.write("shifts = (")
    if binningOffsetsX == None:
        parametersFile.write("2")
    else:
        parametersFile.write(str(binningOffsetsX))
    parametersFile.write(", ")
    if binningOffsetsY == None:
        parametersFile.write("2")
    else:
        parametersFile.write(str(binningOffsetsY))
    parametersFile.write(")\n\n")

    parametersFile.write("# Also shift the binning in age?\n")
    parametersFile.write("refine_age = ")
    if refineAge == None:
        parametersFile.write("False")
    else:
        parametersFile.write(str(refineAge))
    parametersFile.write("\n\n")

    parametersFile.write("# Number of iterations of Poissonian resampling of the observed CMD\n")
    parametersFile.write("nPoisson = ")
    if numberIterations == None:
        parametersFile.write("4")
    else:
        parametersFile.write(str(numberIterations))
    parametersFile.write("\n\n")

    parametersFile.write("# Number of processors to use when running in parallel\n")
    parametersFile.write("nProcessors = ")
    if numberProcessors == None:
        parametersFile.write("mp.cpu_count()")
    else:
        parametersFile.write(str(numberProcessors))
    parametersFile.write("\n")

    parametersFile.close()

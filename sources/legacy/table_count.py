# -*- coding: utf-8 -*-

import numpy


def strictly_increasing(L):
	return all(x<y for x, y in zip(L, L[1:]))


def age_histo(arrData, age):
	h, __ = numpy.histogram(arrData, bins=numpy.array(age)*1e9)
	return h


def met_histo(arrData, met):
	h, __ = numpy.histogram(arrData, bins=met)
	return h
# -*- coding: utf-8 -*-

import numpy
from matplotlib import colors, pyplot


def plot_cmds_label(	observed,
						model,
						xlabelObs='color',
						ylabelObs='magnitude',
						xlabelMod='color',
						ylabelMod='magnitude'):

	observed.axe.set_xlabel(xlabelObs)
	observed.axe.set_ylabel(ylabelObs)
	model.axe.set_xlabel(xlabelMod)
	model.axe.set_ylabel(ylabelMod)

	# refresh Observed
	observed.canvas.draw()

	# refresh Model
	model.canvas.draw()


def plot_cmds(	col1,
				mag1,
				col2,
				mag2,
				activateToolbars,
				observed,
				model,
				crange=(-0.49,2.49),
				mrange=(4.99,-4.99),
				binsize=(0.04,0.03),
				scale=0):

	nbins = [(max(mrange)-min(mrange))/binsize[1], (max(crange)-min(crange))/binsize[0]]
	cmrange = [(min(mrange),max(mrange)),(min(crange),max(crange))]

	num = numpy.size(mag1)

	Ho, __, __ = numpy.histogram2d(mag1, col1, range=cmrange, bins=nbins)
	Hm, __, __ = numpy.histogram2d(mag2[:num], col2[:num], range=cmrange, bins=nbins)

	if scale == 1:      # log
		Ho[Ho < 0.01] = 0.01
		Hm[Hm < 0.01] = 0.01
		HoS = numpy.log10(Ho)
		HmS = numpy.log10(Hm)
		mx  = (HoS.max() if HoS.max() > HmS.max() else HmS.max())
		tcks = ((numpy.array([0., 0.167, 0.333, 0.5, 0.667, 0.833, 1.])*mx)).tolist()
		lab = ['%6.1f' %10**t for t in tcks]
	elif scale == 2:    # sqrt
		HoS = numpy.sqrt(Ho)
		HmS = numpy.sqrt(Hm)
		mx = (HoS.max() if HoS.max() > HmS.max() else HmS.max())
		tcks = ((numpy.array([0., 0.167, 0.333, 0.5, 0.667, 0.833, 1.])*mx)).tolist()
		lab = ['%6.1f' %t**2 for t in tcks]
	else:               # linear
		HoS = Ho
		HmS = Hm
		mx = (HoS.max() if HoS.max() > HmS.max() else HmS.max())
		tcks = ((numpy.array([0., 0.167, 0.333, 0.5, 0.667, 0.833, 1.])*mx)).tolist()
		lab = ['%6.1f' %t for t in tcks]
	norm = colors.Normalize(vmin=-0.1, vmax=mx)



	# Observed CMD settings
	observed.axe.set_ylim(mrange)
	observed.axe.set_xlim(crange)
	observed.axe.autoscale(False)

	# Observed CMD image
	observed.image = observed.axe.imshow(	HoS,
											extent=crange+mrange,
											aspect='auto',
											interpolation='none',
											norm=norm)

	# decide whether to show colorbar or not
	# checkbox checked      --->    Visible
	# checkbox unchecked    --->    Not visible
	if activateToolbars == True:

		# Observed CMD colorbar
		pos = observed.figure.add_axes([0.87, 0.15, 0.02, 0.70])
		observed.colorbar = pyplot.colorbar(	observed.image,
												ticks=tcks,
												cax=pos,
												format='%6.1f')
		observed.colorbar.set_ticklabels(lab)
		observed.colorbar.ax.yaxis.set_ticks_position('right')
		observed.colorbar.ax.yaxis.set_label_position('right')
		observed.colorbar.ax.tick_params(labelsize=10)

		observed.colorBarActivada = 1

	else:
		if observed.colorBarActivada == 1:
			observed.colorbar.remove()
			observed.colorBarActivada = 0

	# refresh Observed
	observed.canvas.draw()





	# Model CMD settings
	model.axe.set_ylim(mrange)
	model.axe.set_xlim(crange)
	model.axe.autoscale(False)

	# Model CMD image
	model.image = model.axe.imshow(	HmS,
									extent=crange+mrange,
									aspect='auto',
									interpolation='none',
									norm=norm)

	# decide whether to show colorbar or not
	# checkbox checked      --->    Visible
	# checkbox unchecked    --->    Not visible
	if activateToolbars == True:

		# Model CMD colorbar
		pos = model.figure.add_axes([0.87, 0.15, 0.02, 0.70])
		model.colorbar = pyplot.colorbar(	model.image,
											cax=pos,
											ticks=tcks,
											format='%6.1f')
		model.colorbar.set_ticklabels(lab)
		model.colorbar.ax.yaxis.set_ticks_position('right')
		model.colorbar.ax.yaxis.set_label_position('right')
		model.colorbar.ax.tick_params(labelsize=10)

		model.colorBarActivada = 1

	else:
		if model.colorBarActivada == 1:
			model.colorbar.remove()
			model.colorBarActivada = 0

	# refresh Model
	model.canvas.draw()


